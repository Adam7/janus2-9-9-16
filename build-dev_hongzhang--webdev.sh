#
# For Hong's own webdev server only, with devel-themer installed and configured.
#
# MUST cd to /data/library/janus2 directory (the repo) to start with, in UNLV Libraries' website case.
#

#!/bin/bash


# preparing shell vars.
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd ..

DIRUP1="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd janus2

DSTAMP=$(date +"%Y%m%d%H%M") 

# This is the actual process of making the website.
echo ">>> Making the new site: build-$DSTAMP ..."
echo
drush make make-build/janus2-dev_hongzhang--webdev.build ../build-$DSTAMP
echo ">>> New site is done ..."
echo


# creating file structure drupal expects
echo ">>> Creating file structure that drupal expects ..."

# create and link files dir
echo ">>> Creating files directories ..."
if [ ! -d ../files ]; then
  # Control will enter here if $DIRECTORY doesn't exist.
  mkdir ../files
  mkdir ../files/files
  mkdir ../files/private
  chmod -R 775 ../files
fi

# linking files directories
echo ">>> Linking files directories ..."
ln -s $DIRUP1/files/files ../build-$DSTAMP/sites/default/files
ln -s $DIRUP1/files/private ../build-$DSTAMP/sites/default/private

# prepare settings file
# if settings.php exists link to it, else copy it and link to it
echo ">>> Preparing settings.php file ..."
if [ -f ../config/settings.php ]
  then
    ln -s $DIRUP1/config/settings.php ../build-$DSTAMP/sites/default/settings.php
  else
    mkdir ../config
    cp ../build-$DSTAMP/sites/default/default.settings.php ../config/settings.php
    chmod 775 ../config/settings.php
#    ln -s $DIR/../config/settings.php ../build-$DSTAMP/sites/default/settings.php
#    echo "linking settings file for beta & www!"
#    ln -s $DIRUP1/config/settings-beta.php ../build-$DSTAMP/sites/beta.library.unlv.edu/settings.php
#    ln -s $DIRUP1/config/settings-www.php ../build-$DSTAMP/sites/www.library.unlv.edu/settings.php
fi

# this may fail if the id running this script does not have sufficient privilege.
# in that case, change file permission manually.
echo ">>> Securing the settings file and parent directory ..."
chmod g-w ../config/settings.php
chmod g-w ../build-$DSTAMP/sites/default


### Dev Only ###
# link custom modules
#echo "linking custom modules..."
#ln -s $DIR/modules ../build-$DSTAMP/sites/all/modules/custom

# link custom themes
#echo "linking custom themes..."
#ln -s $DIR/themes ../build-$DSTAMP/sites/all/themes/custom

# link libraries
if [ -e ../build-$DSTAMP/profiles/janus2/libraries ]; then
  echo ">>> Linking libraries ..."
  ln -s $DIRUP1/build-$DSTAMP/profiles/janus2/libraries $DIRUP1/build-$DSTAMP/sites/all/libraries
#  echo " fixing flexslider lib...";
#  mv $DIRUP1/build-$DSTAMP/sites/all/libraries/flexslider/FlexSlider-2.1/* $DIRUP1/build-$DSTAMP/sites/all/libraries/flexslider/
fi

# linking modules in profile
echo ">>> Linking modules ..."
ln -s $DIRUP1/build-$DSTAMP/profiles/janus2/modules $DIRUP1/build-$DSTAMP/sites/all/modules/in-prof

# customizations to make devel_themer work.
echo ">>> Copying local customization files for devel_themer module ..."
cp $DIRUP1/DRUPAL_REPO/common.inc $DIRUP1/build-$DSTAMP/includes/
cp $DIRUP1/DRUPAL_REPO/devel_themer.js $DIRUP1/build-$DSTAMP/sites/all/modules/dev/devel_themer/


# link to latest build
echo ">>> Linking final drupal site ..."
if [ -L ../drupal ]; then
  rm ../drupal
fi
ln -s build-$DSTAMP ../drupal

echo ">>> All done. enjoy yourself."
echo
