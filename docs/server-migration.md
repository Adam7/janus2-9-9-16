# Server Migration Steps

Use these steps when migrating the code, files and database of the UNLV Libraries main website to another host.

## Overview of moving parts
* Code: the repo with build, make and source files
* Files: drupals files, including images and other content related assets
* Database: the MySQL database running the site

## Steps (in no particular order)
* Take backup of current live site
: I typically use the backup_migrate module interface for ease of use, but this is not required. Any database export and import process will work. 
* Code repo: 
: Make sure your target's repo is up-to-date on prod branch and run a build with: 
`cd /data/library/janus2` and `./build.sh` 
* Transfer files from current production/live server to new target server 
: I typically use a drush rsync command for this like so:
For example of moving from `www-b` to `www-` hosts, I use the following command from the destination (in this case, ran from `www-d` in drupal's root, `/data/library/drupal`):  
`drush rsync @j2.www-b:%files /data/library/files/` **<-- untested**   
*OR*  
I ended up running the following command from the source server:  
`drush rsync sites/default/files @j2.www-a:%files -v`

For the above to work, you must have drush aliases set up properly (*somewhat*) like this:

    cat ~/.drush/j2.aliases.drushrc.php 
    <?php
    $aliases['prod'] = array(
      'uri' => 'beta',
      'root' => '/var/www/builds/janus2_latest-build',
    );

    $aliases['stage'] = array(
      'uri' => '131.216.164.145',
      'root' => '/data/library/drupal',
      'db-url' => 'mysql://lib_stage:[db-password]@libdb.library.unlv.edu:3306/lib_stage',
      'remote-host' => '131.216.162.145',
      'remote-user' => 'cgrzecka',
      'path-aliases' => array(
    #    '%drush' => '/home/LIBRARY/cgrzecka/drush',
    #    '%drush-script' => '/home/LIBRARY/cgrzecka/drush/drush',
        '%dump-dir' => '/home/LIBRARY/cgrzecka/dumps',
        '%files' => '/data/library/drupal/sites/default/files',
    #    '%custom' => '/my/custom/path',
      ),
      'command-specific' => array (
        'sql-sync' => array (
          'no-cache' => TRUE,
        ),
      ),
    );
    $aliases['www-a'] = array(
      'uri' => '131.216.162.73',
      'root' => '/data/library/drupal',
      'db-url' => 'mysql://lib_prod:[db-password]@drupaldb-a.library.unlv.edu:3306/lib_prod',
      'remote-host' => '131.216.162.73',
      'remote-user' => 'cgrzecka',
      'path-aliases' => array(
    #    '%drush' => '/home/LIBRARY/cgrzecka/drush',
    #    '%drush-script' => '/home/LIBRARY/cgrzecka/drush/drush',
        '%dump-dir' => '/home/LIBRARY/cgrzecka/dumps',
        '%files' => '/data/library/drupal/sites/default/files',
    #    '%custom' => '/my/custom/path',
      ),
      'command-specific' => array (
        'sql-sync' => array (
          'no-cache' => TRUE,
        ),
      ),
    );  

And the users keys must be set up as well, TODO: add more info on this later

* Edit settings.php file's $base_url
: in this current example it was changed from:  
`http://www-a.library.unlv.edu`  
to:   
`http://www.library.unlv.edu`

## John's (System's Stuff)
* Update URL Refferer
: `/etc/httpd/conf.d/www.conf`
Need to change refferer to reflect current machine. 
* verify that `/data/library/files/files` and `/data/library/files/private` permission allow group `apache` to write recursivly
