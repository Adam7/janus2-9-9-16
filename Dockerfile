FROM z3cka/drupalio
MAINTAINER Casey Grzecka "c@sey.gr"
EXPOSE 22
CMD ["/bin/bash", "/vagrant/voyage.sh"]