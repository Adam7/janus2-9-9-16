(function($){
  $(document).ready(function(){
    // add show/ hide tabs handler
    $('body.page-node-edit ul.tabs').after('<ul class="tabs primary show-hide"><li class="show-hide">show tabs</li></ul>');
    // animate inactive tabs down
    $('body.page-node-edit ul.tabs li:not(.active)').animate({marginTop: "+=20px"}, 500);
    // click event for showing and hiding tabs
    $('body.page-node-edit ul.tabs li.show-hide').click(function() {
    	$(this).hide();
    	$('body.page-node-edit ul.tabs li:not(.active)').animate({marginTop: "-=20px"}, 500);
    });
  });
})(jQuery);