<?php
/**
 * Implements hook_process_region().
 */
function omegalib_alpha_process_region(&$vars) {
  if ($vars['elements']['#region'] == 'sidebar_second') {
    // check if the current page is a taxonomy view page display
    if (arg(0) == 'taxonomy') {
      // load the current term
      $term = taxonomy_term_load(arg(2));
      // check if the term belongs to the "blog_name" vocabulary
      if ($term->vocabulary_machine_name == 'blog_name') {
        // load theme vars
        $theme = alpha_get_theme();
        // make $feed_icons available to sidebar_second region
        $vars['feed_icons'] = $theme->page['feed_icons'];
      }
    }
  }
}