/*  â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“
    1. GLOBAL
    ......................................  */

html,
body {
    margin: 0;
    padding: 0;
    height: 100%;
}

body {
    font: 16px verdana, sans-serif;
    color: #333;
    background: #fefefe;
    -webkit-hyphens: auto;
    -moz-hyphens: auto;
    -ms-hyphens: auto;
    -o-hyphens: auto;
    hyphens: auto;
}

.page {
    padding: 1.5em 0 0;
}

/*  â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’
    1.1 Typography
    ......................................  */

@font-face {
    font-family: 'FontAwesome';
    src: url('../fonts/fontawesome-webfont.eot?v=3.1.0');
    src: url('../fonts/fontawesome-webfont.eot?#iefix&v=3.1.0') format('embedded-opentype'), url('../fonts/fontawesome-webfont.woff?v=3.1.0') format('woff'), url('../fonts/fontawesome-webfont.ttf?v=3.1.0') format('truetype'), url('../fonts/fontawesome-webfont.svg#fontawesomeregular?v=3.1.0') format('svg');
    /*src: url('../fonts/fontawesome-webfont.eot?v=3.1.0');*/
    font-weight: normal;
    font-style: normal;
}

@font-face {
    font-family: 'Handwriting';
    src: url('../fonts/blokletters-viltstift-webfont.eot');
    src: url('../fonts/blokletters-viltstift-webfont.eot?#iefix') format('embedded-opentype'),
         url('../fonts/blokletters-viltstift-webfont.woff') format('woff'),
         url('../fonts/blokletters-viltstift-webfont.ttf') format('truetype'),
         url('../fonts/blokletters-viltstift-webfont.svg#blokletters_viltstifviltstift') format('svg');
    font-weight: normal;
    font-style: normal;
}

span.glyph {
    font: 1em 'FontAwesome';
    text-transform: none;
    width: 1em;
    display: inline-block;
}

input[type="submit"].glyph {
    font: 1em 'FontAwesome';
}

h1, h2, h3, h4, h5, h6 {
    color: #181818;
}

h1, h2, h3 {
    font-family: georgia,'times new roman',serif;
    font-weight: normal;
    margin-top: 0;
}

h1, h2, h4, h6 {
    font-style: italic;
}

h3, h5 {
    font-weight: bold;
}

h6 {
    font-weight: normal;
}

h1 {
    font-size: 2em;
}

h2 {
    font-size: 1.5em;
}

h3 {
    font-size: 1.17em;
    color: #5c6888;
    color: #777;
}

h4 {
    font-size: 1em;
}

h5 {
    font-size: 1em;
    margin-bottom: 1em;
}

h6 {
    font-size: 1em;
}

p,
ol,
ul,
blockquote,
pre,
code {
    line-height: 1.5;
    margin: 0;
}

blockquote {
    position: relative;
    box-sizing: border-box;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
    clear: both;
    background: #f2f2f2;
    text-align: left;
    margin: 0 0 1.5em;
    padding: 1.5em 1.5em .01em 6em;
}

blockquote p:first-child:before {
    font: 3em 'FontAwesome';
    content: "ï„";
    position: absolute;
    left: .5em;
    top: .5em;
}

blockquote.important p:first-child:before {
    content: "ï±";
}

/*  â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’
    1.2 Images, Links, Lists
    ......................................  */

img {
    border-width: 0;
    border-style: none;
}

a {
    color: #B10202;
    text-decoration: none;
    -webkit-transition: all 0.2s ease;
    -moz-transition: all 0.2s ease;
    -ms-transition: all 0.2s ease;
    -o-transition: all 0.2s ease;
    transition: all 0.2s ease;
}

a:hover {
    text-decoration: underline;
    color: #333;
    -webkit-transition: all 0.2s ease;
    -moz-transition: all 0.2s ease;
    -ms-transition: all 0.2s ease;
    -o-transition: all 0.2s ease;
    transition: all 0.2s ease;
}

ul.no-bullet {
    list-style-type: none;
    padding: 0;
}

ul.bullet li a:before {
    font-family: 'FontAwesome';
    content: "ï€–";
    float: left;
    width: 1em;
    margin-right: .5em;
}

/*  â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’
    1.3 Inputs
    ......................................  */

input {
    outline: 0;
    margin: 0;
    height: 2em;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    border-radius: 0;
}

input[type="text"] {
    width: 100%;
    outline: 0;
    border: 1px solid #999;
    box-shadow: none;
    border-collapse: separate;
    font: 1em verdana, sans-serif;
    padding: .125em .5em;   
    background: -moz-linear-gradient(center top , #FFFFFF,  #EEEEEE 1px, #FFFFFF 20px);    
    background: -webkit-linear-gradient(#EEEEEE,#FFFFFF);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#FBFBFB', endColorstr='#FFFFFF');
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    -moz-box-shadow: 0 0 2px #DDDDDD;
    -webkit-box-shadow: 0 0 2px #DDDDDD;
    box-shadow: 0 0 2px #DDDDDD;
}

div.search-input-wrapper input[type="text"] {
    padding: .125em 3.625em .125em .5em;  
}

input[type="text"]:hover {
    border:1px solid #888;
}

input[type="text"]:focus {
    box-shadow:0 0 2px #888;
}

input[type="submit"] {
    -webkit-appearance: none;
    border: 0;
    outline: 0;
    background: #B10202;
    display: inline-block;
    padding: .25em 1em;
    color: #fff;
    text-decoration: none;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    -moz-box-shadow: 0 1px 3px rgba(0,0,0,0.5);
    -webkit-box-shadow: 0 1px 3px rgba(0,0,0,0.5);
    box-shadow:  0 1px 3px rgba(0,0,0,0.5);
    text-shadow: 0 -1px 1px rgba(0,0,0,0.25);
    border-bottom: 1px solid rgba(0,0,0,0.25);
    position: relative;
    cursor: pointer;
}

input[type="submit"]:hover {
    background: #c50000;
}

/*  â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’
    1.4 Misc.
    ......................................  */

.stilts {
    margin-bottom: 1.5em;
}

.clearfix:before,
.clearfix:after {
    content: " ";
    display: table;
}

.clearfix:after {
    clear: both;
}

.clearfix {
    *zoom: 1;
}

/*  â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“
    2. HEADER
    ......................................  */

#header {
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
    position: relative;
}

@media screen and (min-width: 0) and (max-width: 719px) {
    #header {
        background: #f2f2f2 !important;
    }
    .page.header {
        padding: 0;
    }
}

a.logo {
    float: left;
    background: rgba(255,255,255,.8);
    padding: .9em;
}

@media screen and (min-width: 0) and (max-width: 719px) {
    a.logo {
        width: 100% !important;
        float: none;
        margin: 0 auto;
        display: inline-block;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        -webkit-box-sizing: border-box;
    }
}

@media screen and (min-width: 720px) and (max-width: 1199px) {
    a.logo {
        width: 432px;
    }
}

#logo {
    float: left;
    width: 100%;
    max-width: 540px;
}

#global-header-bar .page {
    padding: 0;
}

#global-header-bar {
    background: #dedede url(../img/header-bkgrd.png) repeat-x top left;
    height: 22px;
    padding: 1px 0;
    border-bottom: solid 1px #999;
}

@media screen and (min-width: 0) and (max-width: 719px) {
    #global-header-bar {
        display: none;
    }
}

#global-header-bar ul { /* links in the unlv header */
    float: right;
    font-size: 10px;
}

#global-header-bar ul li {
    display: inline;
    list-style-type: none;
    margin-left: 10px;
}

#global-header-bar ul a {
    color: #444;
    display: inline-block;
    padding: 5px;
    color: #333;
}

#global-header-bar ul a:after {
    content: ' \00BB';
}

div.header {
    position: relative;
    min-height: 116px;
}

div.content {
    margin-bottom: 1.5em;
}

div.sidebar {
    border-left: 1px solid #4b4b4c;
}

div.sidebar-left,
aside.sidebar-left {
    border-left: 0;
    border-right: 1px solid #4b4b4c;
}

/*  â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’
    2.1 Navigation
    ......................................  */

#nav {
    /*border-top: 1px solid #4b4b4c;*/
    background: rgba(255,255,255,.8);
    border-bottom: 1px solid #ccc;
    position: relative;
    bottom: 0;
    z-index: 999;
    margin: 0 auto;
    display: block;
}

#nav .page {
    padding: 0 0 0;
    line-height: normal;
    position: relative;
    display: block;
}

#main-navigation {
    padding: 0;
    margin: -12px 0 0;
    position: relative;
    bottom: -12px;
    list-style-type: none;
    line-height: normal;
}

#main-navigation li {
    display: inline-block;
    margin-left: 0;
}

#main-navigation li.first {
    margin-left: 0;
}

#main-navigation li:hover a,
#main-navigation li.active a {
    background: #333;
    color: #fff;
}

#main-navigation li a {
    font-family: Georgia, 'Times New Roman', serif;
    height: 1em;
    color: #B10202;
    text-transform: uppercase;
    display: block;
    padding: .5em 1em;
}

#main-navigation li a:hover {
    text-decoration: none;
}

span.arrow-active {
    visibility: hidden;
    display: block;
    margin: 0 auto;
    width: 0; 
    height: 0; 
    border-left: .5em solid transparent;
    border-right: .5em solid transparent;
    border-top: .5em solid #333;
}

#main-navigation li.active a ~ span.arrow-active {
    visibility: visible;
    -webkit-transition: all 0.2s ease;
    -moz-transition: all 0.2s ease;
    -ms-transition: all 0.2s ease;
    -o-transition: all 0.2s ease;
    transition: all 0.2s ease;
}

@media screen and (min-width: 0) and (max-width: 719px) {

    div.sidebar {
        border-left: 0;
    }

    #nav {
        background: transparent;
    }

    #nav .page {
        padding: 0;
    }

    div.header {
        min-height: 0;
    }

    #main-navigation {
        position: relative;
        bottom: auto;
        margin-top: .5em;
    }

    #main-navigation li,
    #main-navigation li.first {
        width: 45%;
        display: inline-block;
        text-align: center;
        margin: 0 .5em;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        -webkit-box-sizing: border-box;
    }

    #main-navigation li a {
        background: rgba(255,255,255,.9);
    }

}

/*  â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“
    3. CONTENT
    ......................................  */

div.content img.sample {
    float: right;
    margin: 0 0 .75em .75em;
    width: 30%;
    border: .25em solid #f2f2f2;
    box-sizing: border-box;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
    -moz-box-shadow:    3px 3px 5px 1px #cecece;
    -webkit-box-shadow: 3px 3px 5px 1px #cecece;
    box-shadow:         3px 3px 5px 1px #cecece;
}

div.sidebar > div.sidebar-block {
    border-top: 1px solid #333;
    padding-top: .83em;
}

div.sidebar > div.sidebar-block:first-child {
    border-top: 0;
    padding: 0;
}

@media screen and (min-width: 0) and (max-width: 719px) {
    div.sidebar > div.sidebar-block:first-child {
        border-top: 1px solid #333;
        padding-top: .83em;
    }
}

div.sidebar ul,
div.sidebar ul li {
    padding: 0;
    list-style-type: none;
}

/*  â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’
    3.1. Home
    ......................................  */

#pinterest {
    margin-bottom: 1.5em;
}

#pinterest .item {
    width: 46%;
    margin-bottom: 4px;

}

#pinterest .grid-sizer {
    width: 48%;
}

#pinterest .item-gutter {
    width: 5%;
}

#pinterest .item img {
    width: 100%;
    border: .25em solid #f2f2f2;
    box-sizing: border-box;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
    -moz-box-shadow:    3px 3px 5px 1px #cecece;
    -webkit-box-shadow: 3px 3px 5px 1px #cecece;
    box-shadow:         3px 3px 5px 1px #cecece;
}

a:hover span.glyph.pinterest {
    color: #cc1f25;
}

/*  â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’
    3.1.1 Home - Search
    ......................................  */

div.multisearch-wrapper {
    margin-top: 1.5em;
}

div.multisearch-wrapper div.search-step:first-child {
    padding-bottom: 0;
    z-index: 99;
    position: relative;
    bottom: -1px;

}

div.hidden {
    display: none;
}

div.radio .ui-button {
    display: inline-block;
    position: relative;
    padding: 0;
    line-height: normal;
    margin: 0 .5em .5em 0;
    cursor: pointer;
    vertical-align: middle;
    text-align: center;
    overflow: visible;
    background: transparent;
    border: 1px dashed #555;
    border-radius: 3px;
}

#radio-search-select .ui-button {
    background: #f9f9f9;
    margin: 0 .5% 0 0;
    width: 19.5%;
    box-sizing: border-box;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
    border: 1px solid #ddd;
    border-radius: 0;
    padding: 0;
}


@media screen and (min-width: 0) and (max-width: 719px) {
    #radio-search-select.cleared .ui-button {
        width: 32.5%;
        margin: 0 .5% 0 0;
    }

    #radio-search-select.cleared .ui-button.clear1 {
        margin-right: 4%;
    }
}

#radio-search-select .ui-button:last-child {
    margin-right: 0;
}

.search-body {
    box-sizing: border-box;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
    padding: .75em;
    background: #ecf0f1;
    position: relative;
    border: 1px solid #ddd;
}

div.radio .ui-button.ui-state-active {
    background: #fff;
    background: #8DB8CD;
    border: 1px solid transparent;
}

#radio-search-select .ui-button.ui-state-active {
    background: #ecf0f1;
    border-bottom-color: #ecf0f1;
    color: #333;
}

.ui-helper-hidden-accessible {
    border: 0;
    clip: rect(0 0 0 0);
    height: 1px;
    margin: -1px;
    overflow: hidden;
    padding: 0;
    position: absolute;
    width: 1px;
}

.ui-button-text-only .ui-button-text {
    padding: .4em 1em;
}

#radio-search-select .ui-button-text {
padding: .5em .75em;
}

.ui-button .ui-button-text {
    display: block;
    line-height: normal;
}

div.search-step {
    /*border-top: 1px dashed #777;*/
    padding-bottom: 1.5em;
}

div.search-step-number {
    text-align: center;
    position: relative;
    top: -1em;
}

div.search-step-number span.step {
    font: 1.17em georgia;
    font-style: italic;
    font-weight: normal;
    display: inline-block;
    text-align: left;
    color: #fff;
    background: #333;
    border: 1px solid #333;
    padding: .2em .8em;
}

div.search-step-number span.arrow-active {
    visibility: visible;
}

div.search-type {
    display: none;
    position: relative;
}

div.search-type.active {
    display: block;
}

div.search-input-wrapper {
    position: relative;
    display: block;
    width: 100%;
    max-width: 340px;
    margin-bottom: 1.5em;
    clear: both;
}

div.search-input-wrapper.database-letter {
    width: 100px;
    padding: 0;
}

div.search-input-wrapper input[type="text"] {
    height: 36px;
}

div.search-input-wrapper input[type="submit"] {
    position: absolute;
    top: 2px;
    right: 2px;
}

#search-database-letter {
    text-transform: uppercase;
}

div.content img.note-select {
    position: absolute;
    margin: -3.75em 0 0 -210px;
}

div.content img.note-navigate {
    position: absolute;
    margin: -9.75em 0 0 380px;
}

@media screen and (min-width: 0) and (max-width: 959px) {
    img.note-select,
    img.note-navigate {
        display: none;
    }
}

.search-wrapper {
    background: #f2f2f2;
    padding: 1.5em;
    margin: 0 0 1.5em 0;
    box-sizing: border-box;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
}

.search {
    position: relative;
    /*box-shadow: 0 0 7px #888;
    -webkit-box-shadow: 0 0 7px #888;*/
}

.search-dd {
    z-index: 999;
    position: absolute;
    margin: 2px 0 0 2px;
}

.search-field {
    width: 100%;
    height: 42px;
    padding-left: .5em;
    border-radius: 0;
}

.search-button {
    z-index: 9999;
    position: absolute;
    right: 0;
    margin: 2px 2px 0 0;
}

table.popular-databases tbody > tr td {
    width: 25%;
}

table.popular-databases tbody > tr:first-child td {
}

table.popular-databases tbody > tr:first-child td > div {
    border: 1px dashed #777;
}

table.popular-databases tbody > tr td img {
    max-width: 100%;
}

/*  â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’
    3.2 Sidebar
    ......................................  */



/*  â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“â–“
    4. FOOTER
    ......................................  */

#footer {
    background: #f2f2f2;
    border-top: solid 1px #e2e2e2;
}

/*  â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’
    4.1 Locations
    ......................................  */

.lib-branch {
    display: none;
}

.lib-branch.active {
    display: block;
}

div.map { /* map */
    display: inline-block;
    width: 100%;
    margin: 0;
    height: 10.15em;
    background-size: cover 100%;
    border: .25em solid #fcfcfc;
    box-sizing: border-box;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
    -moz-box-shadow:    3px 3px 5px 1px #e2e2e2;
    -webkit-box-shadow: 3px 3px 5px 1px #e2e2e2;
    box-shadow:         3px 3px 5px 1px #e2e2e2;
}

.map-lied,
.map-speccol {
    background: transparent url(../img/map-lied.png) center center no-repeat;
}

.map-arch {
    background: transparent url(../img/map-arch.png) center center no-repeat;
}

.map-curr {
    background: transparent url(../img/map-curr.png) center center no-repeat;
}

.map-music {
    background: transparent url(../img/map-music.png) center center no-repeat;
}

.map-law {
    background: transparent url(../img/map-law.png) center center no-repeat;
}

/*  â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’
    4.2 Contact
    ......................................  */

.footer-block {
    display: block;
    margin-bottom: 1.5em;
}

.footer-block ul {
    margin: 0;
    padding: 0;
    list-style-type: none;
}

.footer-block ul li a {
    border-top: 1px dashed #bebebe;
}

.footer-block ul li:last-child a {
    border-bottom: 1px dashed #bebebe;
}

.footer-block ul li a {
    display: block;
}

.footer-block ul li a:hover {
    background: #e6e6e6;
    text-decoration: none;
}

.footer-block ul li a:hover span.facebook {
    color: #3b5999;
}

.footer-block ul li a:hover span.twitter {
    color: #00aced;
}

.footer-block ul li a:hover span.youtube {
    color: #cb322c;
}

.footer-block a.last {
    margin-right: 0;
}

/*  â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’â–’
    4.3 Copyright
    ......................................  */

.copyright {
    display: inline-block;
}

.copyright a {
    color: #333;
}

a.user { /* user login link */
    float: right;
}

@media screen and (min-width: 0) and (max-width: 719px) {
    #footer h2.placeholder {
        display: none;
    }
}