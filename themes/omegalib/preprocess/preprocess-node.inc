<?php

/**
 * Implements hook_preprocess_node().
 */
function omegalib_alpha_preprocess_node(&$vars) {
  // add some vars for printing full name and formated date and permalink in the blog "<footer>" or byline
  // first check if node belongs to BTL blog to replace name with BTL
  if (array_key_exists('field_blog_name', $vars)) {
    if ($vars['field_blog_name']['und'][0]['tid'] == 1065) {
      $vars['author_full_name'] = 'BTL';
    } else {
      $user_loaded = user_load($vars['uid']);
      // check to see if the author of the node has full name value
      if ($user_loaded->field_name_full) {
        $vars['author_full_name'] = $user_loaded->field_name_full['und'][0]['safe_value'];
      } else {
        $vars['author_full_name'] = $user_loaded->name;
      }
    }
  }
  $vars['date_blog'] = format_date($vars['created'], 'custom', 'F j, Y g:i A');
  $vars['permalink'] = l('Permalink', drupal_get_path_alias('node/'.$vars['nid']));

  // provide var for printing slideshows in nodes
  if ($vars['type'] == 'blog') {
    if (array_key_exists('field_slideshow_location', $vars)) {
      if (array_key_exists('und', $vars['field_slideshow_location'])) {
        $vars['slideshow_location'] = $vars['field_slideshow_location']['und'][0]['value'];
      }
    }
  }
}
