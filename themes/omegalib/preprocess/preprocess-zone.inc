<?php

/**
 * Implements hook_preprocess_zone().
 */
function omegalib_alpha_preprocess_zone(&$vars) {
  $context = context_get();
  // TODO: rework this to make it context based, then zone
  switch ($vars['zone']) {
    case 'header':
      if ( isset ($context['context']) ) {
        // provide a zone template suggestions based on context for the header zone
        foreach (array_keys($context['context']) as $context_name) {
          $vars['theme_hook_suggestions'][] = "zone__header__" . strtolower($context_name);
        }
        // provide breadcrumbs to this zone
        $vars['breadcrumbs'] = drupal_get_breadcrumb();
        // use menu_get_active_breadcrumb() for special collections
        if (array_key_exists('special_collections', $context['context'])) {
          $vars['active_breadcrumb'] = menu_get_active_breadcrumb();
          $vars['active_breadcrumb'] = implode(" » ", $vars['active_breadcrumb']);
        } else {
          // make breadcrumbs into a string to print
          $vars['breadcrumbs'] = implode(" » ", $vars['breadcrumbs']);
          // provide title to this zone
        }
        $vars['title'] = drupal_get_title();
      }
      break;
    case 'content':
      // provide a content zone template suggestions based on context
        if ( isset ($context['context']) ) {
            foreach (array_keys($context['context']) as $context_name) {
                $vars['theme_hook_suggestions'][] = "zone__content__" . strtolower($context_name);
            }
        }

      // provide zonebanner for econnections landing and posts
      $blogname = NULL;
      if (arg(0) == 'node') {
        $node = node_load(arg(1));
        if ($node->type == 'blog') {
            $tmp_obj = taxonomy_term_load($node->field_blog_name['und'][0]['tid']);

            if ( is_object ($tmp_obj) )
                $blogname = $tmp_obj->name;
        }
      }
      if (((arg(0) == 'taxonomy') && (arg(2) == '1066')) || ($blogname == 'eConnections') ) {
        $vars['zonebanner'] = theme('zonebanner_econnections');
      } elseif (((arg(0) == 'taxonomy') && (arg(2) == '1065')) || ($blogname == 'Between the Lines') ) {
        $vars['zonebanner'] = theme('zonebanner_btl');
      } elseif (((arg(0) == 'taxonomy') && (arg(2) == '1215')) || ($blogname == "UNLV's Linked Data Project") ) {
        $vars['zonebanner'] = theme('zonebanner_linkeddata');
      } else {
        $vars['zonebanner'] = NULL;
      }
      // hide get-help section on gaming blog
      $vars['gamingblog'] = FALSE;
      if (((arg(0) == 'taxonomy') && (arg(2) == '37')) || ($blogname == 'Center for Gaming Research') ) {
        $vars['gamingblog'] = TRUE;
      }
      break;
    case 'postscript':
      // provide a postscript zone template suggestions based on context
        if ( isset ($context['context']) ) {
            foreach (array_keys($context['context']) as $context_name) {
                $vars['theme_hook_suggestions'][] = "zone__postscript__" . strtolower($context_name);
            }
        }
      break;
  }
}
