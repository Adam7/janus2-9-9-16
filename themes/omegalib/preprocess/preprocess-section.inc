<?php

/**
 * Implements hook_preprocess_section().
 */
function omegalib_alpha_preprocess_section(&$vars) {
  $context = context_get();
  if (array_key_exists('context', $context)) {
    if (array_key_exists('tdrl', $context['context'])) {
      // provide specific section template for TDRL
      $vars['theme_hook_suggestions'][] = 'section__content__tdrl';
    }
  }
  
  // dpm($context, "\$context from section preprocess:");
  // dpm($vars, "\$vars from section preprocess:");
}
