<?php

/**
 * Implements hook_preprocess_page().
 */
function omegalib_alpha_preprocess_page(&$vars) {
  // don't print breadcrumb on econnections & btl blog post pages
  if (arg(0) == 'node') {
    if ( is_object ($vars['node']) )
    {
        if ($vars['node']->type == 'blog') {
            if ($vars['node']->field_blog_name['und'][0]['tid'] == '1066' || $vars['node']->field_blog_name['und'][0]['tid'] == '1065') {
                $vars['breadcrumb'] = 'none';
            }

            // new theme suggestion for gaming blog pages
            if ($vars['node']->field_blog_name['und'][0]['tid'] == '37') {
                // suggest page template
                $vars['theme_hook_suggestions'][] = 'page__gaming_blog';
            }
        }
    }
  }

  // hide title on econnections & btl blog landing
  if (arg(0) == 'taxonomy' && arg(2) == '1066' || arg(0) == 'taxonomy' && arg(2) == '1065') {
    $vars['title'] = 'none';
  }

  // provide a page template suggestion based on context
  $context = context_get();
  // dpm($context, "\$context:");
  if ( isset ($context['context']) ) {
    // dpm($context);
    foreach (array_keys($context['context']) as $context_name) {
      $vars['theme_hook_suggestions'][] = "page__" . strtolower($context_name);
    }
  }
  // provide a vars for the submenus
  $vars['sub_menus'] = get_submenu_vars('menu-special-collections');

  // add a full path to the theme root
  // $vars['theme_path'] = base_path() . drupal_get_path('theme', 'omegalib');

  // do stuff in the speccol front context
  if ( isset ($context['context']) && array_key_exists('special_collections_front', $context['context'])) {
    // load a var to print slideshow block before footer on special collections home
    $vars['footer_slide'] = module_invoke('views', 'block_view', '7cfede7e829de0581780bb068a95d460');
  }
  // remove "header" and "footer" omega sections for TDRL
  if (array_key_exists('tdrl', $context['context'])) {
    unset($vars['page']['header']);
    unset($vars['page']['footer']);
  }
  // add a var for the path to this theme
  $vars['theme_path'] = base_path() . drupal_get_path('theme', 'omegalib');
  // dpm($vars, "\$vars:");
}
