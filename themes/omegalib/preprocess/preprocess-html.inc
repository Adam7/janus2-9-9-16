<?php

/**
 * Implements hook_preprocess_html().
 */
function omegalib_alpha_preprocess_html(&$vars) {
  // Conditional Styles for IE8 and below
  $csspaths = array(
    'narrow' => '/css/omegalib-alpha-default-narrow.css',
    'ie8' => '/css/ie8-and-below.css',
  );
  $cssoptions = array(
    'type' => 'file',
    'group' => 1000,
    'media' => 'all',
    'browsers' => array('IE' => 'lte IE 8', '!IE' => FALSE),
    'preprocess' => FALSE
  );
  foreach ($csspaths as $csspath) {
    drupal_add_css(drupal_get_path('theme', 'omegalib'). $csspath, $cssoptions);
  }

  // replace body classes for the gaming blog
  if (arg(0) == 'taxonomy' && arg(2) == '37') {
    $vars['attributes_array']['alink'] = '#000000';
    $vars['attributes_array']['bgcolor'] = '#ffffff';
    $vars['attributes_array']['class'] = 'hasGoogleVoiceExt gaming';
    $vars['attributes_array']['onload'] = 'preloadImages();';
    $vars['attributes_array']['vlink'] = '#333333';
  }
  // provide html template based on node's field_blog_name term id
  if ($node = menu_get_object()) {
//    $vars['theme_hook_suggestions'][] = 'html__'. str_replace('_', '--', $node->field_blog_name['und'][0]['tid']);
    if (isset($node->field_blog_name)) {
      $vars['theme_hook_suggestions'][] = 'html__node_term_'.$node->field_blog_name['und'][0]['tid'];
    }
  }

  // add an underscore before all page titles so that Drupal pares are easily identified
  $vars['head_title'] = '_'. $vars['head_title'];

  // add arch_front context class on the body element
  $context = context_get();
  if (array_key_exists('context', $context)) {
    // do stuff based on context
    switch ($context) {
      case array_key_exists('arch', $context['context']):
        // theme main menu for arch
        $vars['menu_main_arch'] = theme(
          'links', 
          array(
            'links' => menu_navigation_links('menu-arch-menu'),
            'attributes' => array('id' => 'main-navigation'),
          )
        );        
        break;
      case array_key_exists('arch_front', $context['context']):
        $vars['attributes_array']['class'][] = 'context-arch-front';
        break;
    }
    // html template suggestions based on context
    foreach ($context['context'] as $key => $value) {
      $vars['theme_hook_suggestions'][] = 'html__context__'. $value->name;
    }
  }
  // add a var for the path to this theme
  $vars['theme_path'] = base_path() . drupal_get_path('theme', 'omegalib');

  // theme main menu for special collections
  $vars['menu_main_speccol'] = theme(
    'links', 
    array(
      'links' => menu_navigation_links('menu-special-collections'),
      'attributes' => array('id' => 'main-menu'),
    )
  );
  // dpm($vars);
}
