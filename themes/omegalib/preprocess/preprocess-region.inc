<?php

/**
 * Implements hook_preprocess_region().
 */
function omegalib_alpha_preprocess_region(&$vars) {
  $context = context_get();

    if ( isset ($context['context']) )
        $context_ok = TRUE;
    else
        $context_ok = FALSE;


    if ( $context_ok )
    {
        switch ($context) {
            case array_key_exists('special_collections', $context['context']):
                // provide "Explore" menu var
                $vars['sub_menus'] = get_submenu_vars('menu-special-collections');

            case array_key_exists('special_collections_front', $context['context']):
                // load a var to print speccol news (blog entries)
                $vars['speccol_news'] = views_embed_view('blog_entries', 'block', 35);
        }
    }

  // TODO: make the a switch statement based on context first... then region stuff that calls a function(etc.)
  if ($vars['region'] == 'menu') {
    // provide arch menu for arch pages
    $vars['main_menu_arch'] = menu_navigation_links('menu-arch-menu');
    // provide a region menu & content template suggestion based on context
    if ( $context_ok ) {
      foreach (array_keys($context['context']) as $context_name) {
        $vars['theme_hook_suggestions'][] = "region__menu__" . strtolower($context_name);
      }
    }
  }

  // provide region sidebar_first with a context template
  if ($vars['region'] == 'sidebar_first') {
    if ( $context_ok ) {
        foreach (array_keys($context['context']) as $context_name) {
            $vars['theme_hook_suggestions'][] = "region__sidebar_first__" . strtolower($context_name);
        }
    }
  }

  // provide region content with a context template
  if ($vars['region'] == 'content') {
    if ( $context_ok ) {
        foreach (array_keys($context['context']) as $context_name) {
            $vars['theme_hook_suggestions'][] = "region__content__" . strtolower($context_name);
        }
    }
  }

  // provide region sidebar_second with a context template
  if ( $context_ok && $vars['region'] == 'sidebar_second') {
    // vars for params to pass to quick links section for special collections
    if (array_key_exists('special_collections', $context['context'])) {
      $params_menu_quick_links = array(
        'min_depth' => 2,
        'max_depth' => 2,
        'conditions' => array('plid' => '2247'),
      );
      // print the block title and decendents of active tree of the menu for the sidebar
      $active_tree = menu_tree_page_data('menu-special-collections');
      foreach($active_tree as $key => $value) {
        if ($value['link']['in_active_trail'] != 1) {
          unset($active_tree[$key]);
        };
      }
      // get title for the above block
      $first = reset($active_tree);
      $vars['active_tree_title'] = $first['link']['link_title'];
      $vars['active_tree_decendents'] = $first['below'];

      // get quicklinks menu by calling this once from this region
      $vars['sub_menu'] = get_submenu_vars('menu-special-collections');
    }
    
    foreach (array_keys($context['context']) as $context_name) {
      $vars['theme_hook_suggestions'][] = "region__sidebar_second__" . strtolower($context_name);
    }

    // vars for blocks in this region depending on context
    if (array_key_exists('arch_front', $context['context'])) {
      // vars for blocks that exist in arch_front context
      $vars['block_subpages'] = _block_get_renderable_array(
        _block_render_blocks(
          array(block_load('library_content', 'subpages'))
        )
      );
    } elseif (array_key_exists('arch', $context['context'])) {
      // vars for blocks in arch context
      // subpages block
      $vars['block_subpages'] = _block_get_renderable_array(
        _block_render_blocks(
          array(block_load('library_content', 'subpages'))
        )
      );
    }
  }
}
