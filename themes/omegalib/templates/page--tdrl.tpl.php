<!-- Page Title
		============================================= -->
<section id="page-title">
  <div class="container clearfix">
    <div class="col-md-12">
      <h1><?php print $title; ?></h1>
      <?php print $breadcrumb; ?>
      <!--<ol class="breadcrumb">-->
      <!--  <li><a href="#">Home</a></li>-->
      <!--  <li><a href="#">Education Databases</a></li>-->
      <!--</ol>-->
    </div>
  </div>
</section>
<!-- #page-title end -->

<!-- Content
============================================= -->
<section id="content">
  <div class="content-wrap">
    <div class="container clearfix">
      <?php print render($page['content']) ?>
    </div>
    <!-- container clearfix end -->
  </div>
  <!-- content-wrap end -->
</section>
<!-- #content end -->
<div class="color-line"></div>
