<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="UNLV University Libraries" />

	<!-- Stylesheets
	============================================= -->
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="<?php print $theme_path; ?>/css/tdrl/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="<?php print $theme_path; ?>/css/tdrl/style.css" type="text/css" />
	<link rel="stylesheet" href="<?php print $theme_path; ?>/css/tdrl/dark.css" type="text/css" />
	<link rel="stylesheet" href="<?php print $theme_path; ?>/css/tdrl/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="<?php print $theme_path; ?>/css/tdrl/animate.css" type="text/css" />
	<link rel="stylesheet" href="<?php print $theme_path; ?>/css/tdrl/magnific-popup.css" type="text/css" />

	<link rel="stylesheet" href="<?php print $theme_path; ?>/css/tdrl/responsive.css" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<!--[if lt IE 9]>
		<script src="https://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->
    <link rel="stylesheet" href="<?php print $theme_path; ?>/css/tdrl/colors.css" type="text/css" />

    <!-- Search bar scripts -->
    <link type="text/css" rel="stylesheet" href="https://www.library.unlv.edu/main_extra.css" media="all" />
    <script type="text/javascript">window.jQuery || document.write("<script src='https://www.library.unlv.edu/sites/all/modules/in-prof/dev/jquery_update/replace/jquery/1.7/jquery.min.js'>\x3C/script>");</script>
    <script type="text/javascript" src="https://www.library.unlv.edu/misc/jquery.once.js?v=1.2"></script>
    <script type="text/javascript" src="https://www.library.unlv.edu/profiles/janus2/themes/omegalib/js/universitylibraries/select2.min.js?nihi05"></script>
    <script type="text/javascript" src="https://www.library.unlv.edu/profiles/janus2/themes/omegalib/js/universitylibraries/universitylibraries.js?nihi05"></script>
    <link rel="stylesheet" href="<?php print $theme_path; ?>/css/tdrl/additional-styles.css" type="text/css" />
    <!-- End Seach bar scripts -->

	<!-- Document Title
	============================================= -->
	<title>TDRL - UNLV University Libraries</title>

</head>

<body class="no-transition stretched">


	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Top Bar
		============================================= -->
		<div id="top-bar">

			<div class="container clearfix">

				<div class="col_half nobottommargin">

					<!-- top bar left -->
                    <div class="top-links">
                    <a href="#" class="standard-logo"><img src="<?php print $theme_path; ?>/images/unlv-sm.png" alt="unlv Logo" width="47px"></a>
                    </div>

				</div>

				<div class="col_half fright col_last nobottommargin">

                    <!-- top bar right -->
                    <div class="top-links">
						<ul>
							<li><a href="https://webcampus.unlv.edu/">WebCampus <i class="icon-line-fast-forward"></i></a></li>
							<li><a href="https://my.unlv.nevada.edu/psp/lvporprd/EMPLOYEE/EMPL/h/?tab=PAPP_GUEST">MyUNLV <i class="icon-line-fast-forward"></i></a></li>
							<li><a href="http://rebelmail.unlv.edu/">Rebelmail <i class="icon-line-fast-forward"></i></a></li>
						</ul>
					</div>





				</div>

			</div>

		</div><!-- #top-bar end -->

		<!-- Header
		============================================= -->
		<header id="header" class="sticky-style-2">

			<div class="container clearfix">




				<!-- Logo
				============================================= -->
				<div class="col-md-6" id="">
						<a href="http://library.unlv.edu/tdrl" class="" data-dark-logo="<?php print $theme_path; ?>/images/TDRL2.png"><img class="img-responsive" src="<?php print $theme_path; ?>/images/TDRL2.png" alt="TDRL Logo"></a>
				</div><!-- #logo end -->






		<div class="col-md-6 col-last">



<div class="search-tool-wrapper clearfix">
    <p class="more-research">
        <strong>Research Links: </strong><a href="https://www.library.unlv.edu/search/databases/index.html">List of Databases</a>, <a href="http://www.library.unlv.edu/services/reserves">Course Reserves</a>
    </p>

    <form id="ui_element" class="sb_wrapper clearfix" action="https://unlv.summon.serialssolutions.com/search">
        <p>
        <span id="sb_arrow" class="sb_down"></span>
        <input class="sb_input" type="text" name="s.q" autocomplete="off">
        <input class="sb_search" type="submit" value="" target="_blank">
        </p>

        <ul class="sb_dropdown" style="display: none;">
            <li class="quick-search clearfix" data-value="https://unlv.summon.serialssolutions.com/search">
                <span class="search-type">Quick Search:</span>
                <ul>
                    <li><a class="active" href="" data-value="summonAll">Keyword</a></li>
                    <br class="hidden-lg hidden-md"/>
                    <li><a href="" data-value="summonBooks">Books</a></li>
                    <br class="hidden-lg hidden-md"/>
                    <li><a href="" data-value="summonArticles">Articles</a></li>
                    <br class="hidden-lg hidden-md"/>
                </ul>

                <div class="search-specific" hidden="" style="display:none;">
                    <input checked="checked" id="summonAll" name="s.cmd" type="radio" value="">
                    <input id="summonBooks" name="s.cmd" type="radio" value="addFacetValueFilters(ContentType,Book / eBook,f)">
                    <input id="summonArticles" name="s.cmd" type="radio" value="addFacetValueFilters(ContentType,Journal Article,f)">

                </div>
            </li>

            <li class="catalog-search clearfix">
                <span class="search-type">Catalog:</span>
                <ul>
                    <li><a href="" data-value="https://webpac.library.unlv.edu/search~S1/X">Keyword</a></li>
                    <br class="hidden-lg hidden-md"/>
                    <li><a href="" data-value="https://webpac.library.unlv.edu/search~S1/t">Title</a></li>
                    <br class="hidden-lg hidden-md"/>
                    <li><a href="" data-value="https://webpac.library.unlv.edu/search~S1/a">Author</a></li>
                    <br class="hidden-lg hidden-md"/>
                </ul>
            </li>

            <li class="site-search clearfix" data-value="https://www.library.unlv.edu/search/googleresults.html">
                <span class="search-type">Library Information:</span>
                <ul>
                    <li><a href="" name="q">Library Information</a></li>
                </ul>

                <div class="search-specific" hidden="" style="display:none;">
                    <input name="cx" value="001581898298350756962:-ch-cv_3h1k" disabled="">
                    <input name="cof" value="FORID:10" disabled="">
                    <input name="ie" value="UTF-8" disabled="">
                </div>
            </li>
        </ul>
    </form>




</div>



        </div>






			</div>

			<div id="header-wrap">

				<!-- Primary Navigation
				============================================= -->
				<nav id="primary-menu" class="style-2">

					<div class="container clearfix">

						<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

						<ul>
							<li><a href="https://www.library.unlv.edu/tdrl"><div>Home</div></a>

							</li>
							<li><a href="https://www.library.unlv.edu/tdrl/research"><div>Research</div></a>
								<ul>
									<li><a href="https://www.library.unlv.edu/tdrl/research/education-databases"><div><i class="icon-data"></i>Education Databases</div></a></li>
									<li><a href="https://www.library.unlv.edu/tutorials"><div><i class="icon-pencil"></i>Tutorials</div></a></li>
									<li><a href="https://www.library.unlv.edu/tdrl/research/curriculum-standards"><div><i class="icon-globe"></i>Curriculum Standards</div></a></li>
									<li><a href="https://www.library.unlv.edu/tdrl/research/praxis"><div><i class="icon-line-layout"></i>Praxis</div></a></li>
								</ul>
							</li>
							<li><a href="https://www.library.unlv.edu/tdrl/collections/"><div>Collections</div></a>
								<ul>
									<li><a href="https://www.library.unlv.edu/tdrl/collections/professional-development"><div><i class="icon-users"></i>Professional Development</div></a></li>
									<li><a href="https://www.library.unlv.edu/tdrl/collections/picture-books"><div><i class="icon-picture"></i>Picture Books</div></a></li>
									<li><a href="https://www.library.unlv.edu/tdrl/collections/early-readers"><div><i class="icon-book2"></i>Early Readers</div></a></li>
									<li><a href="https://www.library.unlv.edu/tdrl/collections/juvenile-fiction"><div><i class="icon-android"></i>Juvenile Fiction</div></a></li>
                                    <li><a href="https://www.library.unlv.edu/tdrl/collections/juvenile-nonfiction"><div><i class="icon-align-justify"></i>Juvenile Nonfiction</div></a></li>
                                    <li><a href="https://www.library.unlv.edu/tdrl/collections/graphic-novels"><div><i class="icon-photo"></i>Graphic Novels</div></a></li>
                                    <li><a href="https://www.library.unlv.edu/tdrl/collections/textbooks"><div><i class="icon-book3"></i>Textbooks</div></a></li>
                                    <li><a href="https://www.library.unlv.edu/tdrl/collections/bilingual"><div><i class="icon-comments-alt"></i>Bilingual</div></a></li>
                                    <li><a href="https://www.library.unlv.edu/tdrl/collections/kits"><div><i class="icon-suitcase"></i>Kits</div></a></li>
								</ul>
							</li>
							<li><a href="https://www.library.unlv.edu/tdrl/services"><div>Services</div></a>
								<ul>
									<li><a href="https://www.library.unlv.edu/tdrl/services/borrowing-privileges"><div><i class="icon-download"></i>Borrowing Privileges</div></a></li>
									<li><a href="https://www.library.unlv.edu/tdrl/services/course-reserves"><div><i class="icon-book"></i>Course Reserves</div></a></li>
									<li><a href="https://www.library.unlv.edu/tdrl/services/teacher-preparation-lab-services"><div><i class="icon-lab"></i>Teacher Preparation Lab/Services</div></a></li>
									<li><a href="https://www.library.unlv.edu/tdrl/services/learning-spaces-technology"><div><i class="icon-tablet"></i>Learning Spaces & Technology</div></a></li>
                                    <li><a href="https://www.library.unlv.edu/tdrl/services/library-instruction-sessions"><div><i class="icon-pencil"></i>Library Instruction Sessions</div></a></li>
									<li><a href="https://www.library.unlv.edu/tdrl/services/meet-with-librarian"><div><i class="icon-user"></i>Meet with a Librarian</div></a></li>
								</ul>
							</li>

							<li class=""><a href="https://www.library.unlv.edu/tdrl/book-lists-awards"><div>Book Lists & Awards</div></a>
							</li>

							<li><a href="https://www.library.unlv.edu/tdrl/about"><div>About</div></a>
							</li>

							<li class=""><a href="https://www.library.unlv.edu/tdrl/contact"><div>Contact</div></a>
							</li>
						</ul>

					</div>

				</nav><!-- #primary-menu end -->

			</div>

		</header><!-- #header end -->

        <?php print $page; ?>

		<!-- Footer
		============================================= -->
		<footer id="footer" class="dark">
            <div class="content-wrap">

               <div class="container clearfix">
               <div class="col-md-6">
                        <select class="form-control" id="target">
                        <option value="content_1" selected="selected">Teacher Library</option>
                        <option value="content_2">Lied Library</option>
                        <option value="content_3">Special Collections</option>
                        <option value="content_4">Architecture Library</option>
                        <option value="content_5">Music Library</option>
                        <option value="content_6">Law Library</option>
                        </select>
                </div>
                </div>
                <br>

                <div class="container clearfix even-panels">

                       <!-- Teacher Library -->
                       <div class="vis" id="content_1">
                                <div class="col-md-3">
                                <div class="panel panel-default">
							    <div class="panel-body">
                                <h4 class="list-group-item-heading"><i class="icon-book2"></i>&nbsp; Teacher Library</h4><br>
                                    <div style="background: url('<?php print $theme_path; ?>/images/world-map.png') no-repeat center center; background-size: 100%;">
									<address>
										<strong>4505 South Maryland Pkwy.</strong><br>
										Box 453009<br>
										Las Vegas, Nevada<br>
                                        89154-3009<br>
									</address>
                                    <p>
                                    <a href="https://www.library.unlv.edu/tdrl">Library Site &nbsp; <i class="icon-line-arrow-right"></i></a><br>
									<abbr title="Phone Number"></abbr> (702) 895-3593<br>
                                    </p>
                                    <p><strong>Today's Hours:</strong><br>
                                    8AM - 8PM</p>
								    </div>
							    </div>
						        </div>
                                </div>

                                <div class="col-md-3">
                                <div class="panel panel-default">
							    <div class="panel-body">
                                <h4 class="list-group-item-heading"><i class="icon-map"></i>&nbsp; Location</h4><br>
                                <img src="<?php print $theme_path; ?>/images/tdrl-map.jpg" alt=""><br><br>
                                <h5>More Information</h5>
								<ul class="widget_links">
									<li><a href="https://www.library.unlv.edu/services/hours">Future Library Hours</a></li>
									<li><a href="https://unlv.campusdish.com/Locations/TheCoffeeBeanAndTeaLeaf.aspx">Book 'n Bean Hours</a></li>
									<li><a href="http://www.unlv.edu/parking">Parking Information</a></li>
                                    <br>
								</ul>
							    </div>
						        </div>
                                </div>
                       </div> <!-- end teacher Library -->

                       <!-- Lied Library -->
                       <div class="inv" id="content_2">
                                <div class="col-md-3">
                                <div class="panel panel-default">
							    <div class="panel-body">
                                <h4 class="list-group-item-heading"><i class="icon-book"></i>&nbsp; Lied Library</h4><br>
                                    <div style="background: url('<?php print $theme_path; ?>/images/world-map.png') no-repeat center center; background-size: 100%;">
									<address>
										<strong>4505 South Maryland Pkwy.</strong><br>
										Box 457001<br>
										Las Vegas, Nevada<br>
                                        89154-7001<br>
									</address>
                                    <p>
                                    <a href="https://www.library.unlv.edu/">Library Site &nbsp; <i class="icon-line-arrow-right"></i></a><br>
									<abbr title="Phone Number"></abbr> (702) 895-2111<br>
                                    </p>
                                    <p><strong>Today's Hours:</strong><br>
                                    7:30AM - 10PM</p>
								    </div>
							    </div>
						        </div>
                                </div>

                                <div class="col-md-3">
                                <div class="panel panel-default">
							    <div class="panel-body">
                                <h4 class="list-group-item-heading"><i class="icon-map"></i>&nbsp; Location</h4><br>
                                <img src="<?php print $theme_path; ?>/images/lied-map.jpg" alt=""><br><br>
                                <h5>More Information</h5>
								<ul class="widget_links">
									<li><a href="https://www.library.unlv.edu/services/hours">Future Library Hours</a></li>
									<li><a href="https://unlv.campusdish.com/Locations/TheCoffeeBeanAndTeaLeaf.aspx">Book 'n Bean Hours</a></li>
									<li><a href="http://www.unlv.edu/parking">Parking Information</a></li>
                                    <br>
								</ul>
                                </div>
						        </div>
                                </div>
                       </div> <!-- end Lied Library -->

                       <!-- Special Collections -->
                       <div class="inv" id="content_3">
                                <div class="col-md-3">
                                <div class="panel panel-default">
							    <div class="panel-body">
                                <h4 class="list-group-item-heading"><i class="icon-book3"></i>&nbsp; Special Collections</h4><br>
                                    <div style="background: url('<?php print $theme_path; ?>/images/world-map.png') no-repeat center center; background-size: 100%;">
									<address>
										<strong>4505 South Maryland Pkwy.</strong><br>
										Box 457010<br>
										Las Vegas, Nevada<br>
                                        89154-7010<br>
									</address>
                                    <p>
                                    <a href="https://www.library.unlv.edu/speccol">Library Site &nbsp; <i class="icon-line-arrow-right"></i></a><br>
									<abbr title="Phone Number"></abbr> (702) 895-2234<br>
                                    </p>
                                    <p><strong>Today's Hours:</strong><br>
                                    9AM - 5PM</p>
								    </div>
                                </div>
						        </div>
                                </div>

                                <div class="col-md-3">
                                <div class="panel panel-default">
							    <div class="panel-body">
                                <h4 class="list-group-item-heading"><i class="icon-map"></i>&nbsp; Location</h4><br>
                                <img src="<?php print $theme_path; ?>/images/lied-map.jpg" alt=""><br><br>
                                <h5>More Information</h5>
								<ul class="widget_links">
									<li><a href="https://www.library.unlv.edu/services/hours">Future Library Hours</a></li>
									<li><a href="https://unlv.campusdish.com/Locations/TheCoffeeBeanAndTeaLeaf.aspx">Book 'n Bean Hours</a></li>
									<li><a href="http://www.unlv.edu/parking">Parking Information</a></li>
                                    <br>
								</ul>
							    </div>
						        </div>
                                </div>
                       </div> <!-- end Special Collections -->

                       <!-- Arch Library -->
                       <div class="inv" id="content_4">
                                <div class="col-md-3">
                                <div class="panel panel-default">
							    <div class="panel-body">
                                <h4 class="list-group-item-heading"><i class="icon-book2"></i>&nbsp; Architecture Library</h4><br>
                                    <div style="background: url('<?php print $theme_path; ?>/images/world-map.png') no-repeat center center; background-size: 100%;">
									<address>
										<strong>4505 South Maryland Pkwy.</strong><br>
										Box 454049<br>
										Las Vegas, Nevada<br>
                                        89154-4049<br>
									</address>
                                    <p>
                                    <a href="https://www.library.unlv.edu/arch">Library Site &nbsp; <i class="icon-line-arrow-right"></i></a><br>
									<abbr title="Phone Number"></abbr> (702) 895-1959<br>
                                    </p>
                                    <p><strong>Today's Hours:</strong><br>
                                    9AM - 5PM</p>
								    </div>
							    </div>
						        </div>
                                </div>

                                <div class="col-md-3">
                                <div class="panel panel-default">
							    <div class="panel-body">
                                <h4 class="list-group-item-heading"><i class="icon-map"></i>&nbsp; Location</h4><br>
                                <img src="<?php print $theme_path; ?>/images/arch-map.jpg" alt=""><br><br>
                                <h5>More Information</h5>
								<ul class="widget_links">
									<li><a href="https://www.library.unlv.edu/services/hours">Future Library Hours</a></li>
									<li><a href="https://unlv.campusdish.com/Locations/TheCoffeeBeanAndTeaLeaf.aspx">Book 'n Bean Hours</a></li>
									<li><a href="http://www.unlv.edu/parking">Parking Information</a></li>
                                    <br>
								</ul>
							    </div>
						        </div>
                                </div>
                       </div> <!-- end Arch Library -->

                       <!-- Music Library -->
                       <div class="inv" id="content_5">
                                <div class="col-md-3">
                                <div class="panel panel-default">
							    <div class="panel-body">
                                <h4 class="list-group-item-heading"><i class="icon-book"></i>&nbsp; Music Library</h4><br>
                                    <div style="background: url('<?php print $theme_path; ?>/images/world-map.png') no-repeat center center; background-size: 100%;">
									<address>
										<strong>4505 South Maryland Pkwy.</strong><br>
										Box 457002<br>
										Las Vegas, Nevada<br>
                                        89154-7002<br>
									</address>
                                    <p>
                                    <a href="https://www.library.unlv.edu/music">Library Site &nbsp; <i class="icon-line-arrow-right"></i></a><br>
									<abbr title="Phone Number"></abbr> (702) 895-2541<br>
                                    </p>
                                    <p><strong>Today's Hours:</strong><br>
                                    10AM - 6PM</p>
								    </div>
							    </div>
						        </div>
                                </div>

                                <div class="col-md-3">
                                <div class="panel panel-default">
							    <div class="panel-body">
                                <h4 class="list-group-item-heading"><i class="icon-map"></i>&nbsp; Location</h4><br>
                                <img src="<?php print $theme_path; ?>/images/music-map.jpg" alt=""><br><br>
                                <h5>More Information</h5>
								<ul class="widget_links">
									<li><a href="https://www.library.unlv.edu/services/hours">Future Library Hours</a></li>
									<li><a href="https://unlv.campusdish.com/Locations/TheCoffeeBeanAndTeaLeaf.aspx">Book 'n Bean Hours</a></li>
									<li><a href="http://www.unlv.edu/parking">Parking Information</a></li>
                                    <br>
								</ul>
							    </div>
						        </div>
                                </div>
                       </div> <!-- end Music Library -->

                       <!-- Law Library -->
                       <div class="inv" id="content_6">
                                <div class="col-md-3">
                                <div class="panel panel-default">
							    <div class="panel-body">
                                <h4 class="list-group-item-heading"><i class="icon-book3"></i>&nbsp; Law Library</h4><br>
                                    <div style="background: url('<?php print $theme_path; ?>/images/world-map.png') no-repeat center center; background-size: 100%;">
									<address>
										<strong>4505 South Maryland Pkwy.</strong><br>
										Box 451003<br>
										Las Vegas, Nevada<br>
                                        89154-1003<br>
									</address>
                                    <p>
                                    <a href="http://law.unlv.edu/law-library/home.html">Library Site &nbsp; <i class="icon-line-arrow-right"></i></a><br>
									<abbr title="Phone Number"></abbr> (702) 895-3671<br>
                                    </p>
                                    <p><strong>Today's Hours:</strong><br>
                                    10AM - 6PM</p>
								    </div>
							    </div>
						        </div>
                                </div>

                                <div class="col-md-3">
                                <div class="panel panel-default">
							    <div class="panel-body">
                                <h4 class="list-group-item-heading"><i class="icon-map"></i>&nbsp; Location</h4><br>
                                <img src="<?php print $theme_path; ?>/images/law-map.jpg" alt=""><br><br>
                                <h5>More Information</h5>
								<ul class="widget_links">
									<li><a href="https://www.library.unlv.edu/services/hours">Future Library Hours</a></li>
									<li><a href="https://unlv.campusdish.com/Locations/TheCoffeeBeanAndTeaLeaf.aspx">Book 'n Bean Hours</a></li>
									<li><a href="http://www.unlv.edu/parking">Parking Information</a></li>
                                    <br>
								</ul>
							    </div>
						        </div>
                                </div>
                       </div> <!-- end Law Library -->


                                <div class="col-md-3">
                                <div class="panel panel-default">
							    <div class="panel-body">
                                <h4 class="list-group-item-heading"><i class="icon-comment"></i>&nbsp; Contact</h4><br>
                                <h5>Ask Us!</h5>
                                <a href="https://www.library.unlv.edu/#"><i class="icon-chat"></i>&nbsp; Chat</a><br>
                                <i class="icon-phone2"></i>&nbsp; Text: (702) 945-0822<br>
                                <i class="icon-call"></i>&nbsp;&nbsp; Phone: (702) 895-2111<br>
								<a href="https://www.library.unlv.edu/ask/email.php"><i class="icon-envelope2"></i>&nbsp; Email</a><br><br>
                                <h5>Social Media</h5>
                                <a href="https://www.facebook.com/unlvlib"><i class="icon-facebook"></i>&nbsp;&nbsp; Facebook</a><br>
                                <a href="https://twitter.com/unlvlibraries"><i class="icon-twitter2"></i>&nbsp; Twitter</a><br>
                                <a href="http://www.youtube.com/user/unlvlibraries"><i class="icon-youtube"></i>&nbsp; YouTube</a><br>
                                <a href="http://instagram.com/unlvlibraries"><i class="icon-instagram2"></i>&nbsp; Instagram</a><br>
                                 <a href="https://www.library.unlv.edu/contact/social_media">All Social Media</a>
                                 <br><br>
							    </div>
						        </div>
                                </div>

                                <div class="col-md-3">
                                <div class="panel panel-default">
							    <div class="panel-body">

                                  <h5>Directories</h5>
								<ul class="widget_links">
									<li><a href="https://www.library.unlv.edu/contact/librarians_by_subject">Librarians by Subject</a></li>
									<li><a href="https://www.library.unlv.edu/about/staff/libstafinfo.php?style=2">Staff Directory</a></li><br>
								</ul>
                                <h5>Employment</h5>
								<ul class="widget_links">
									<li><a href="https://www.library.unlv.edu/employment">Full Time</a></li>
									<li><a href="https://www.library.unlv.edu/employment/student.html">Student</a></li><br>
								</ul>
                                <h5>Feedback</h5>
								<ul class="widget_links">
									<li><a href="https://www.library.unlv.edu/comments.php">Provide Feedback</a></li>
								</ul>
                                <br>
                                </div>
						        </div>
                                </div>
                </div>
            </div>



                    <div id="copyrights">
				    <div class="container clearfix">
					<div class="col_half">
						&copy; 2016 University of Nevada, Las Vegas<br>
					</div>
                    </div>
                    </div>

		</footer><!-- #footer end -->

	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- External JavaScripts
	============================================= -->
	<script type="text/javascript" src="<?php print $theme_path; ?>/js/tdrl/jquery.js"></script>
	<script type="text/javascript" src="<?php print $theme_path; ?>/js/tdrl/plugins.js"></script>

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="<?php print $theme_path; ?>/js/tdrl/functions.js"></script>


       <script>
            document
                .getElementById('target')
                .addEventListener('change', function () {
                    'use strict';
                    var vis = document.querySelector('.vis'),
                        target = document.getElementById(this.value);
                    if (vis !== null) {
                        vis.className = 'inv';
                    }
                    if (target !== null ) {
                        target.className = 'vis';
                    }
            });
        </script>


</body>
</html>
