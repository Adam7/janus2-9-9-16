<?php if ($wrapper): ?><div<?php print $attributes; ?>><?php endif; ?>  
  <div<?php print $content_attributes; ?>>
    <?php if ($gamingblog != TRUE): ?>
        <div id="help-icon">
            <a href="http://ask.library.unlv.edu/"><img src="/images/gethelp.png" width="85px"></a>
        </div>
    <?php endif; ?>
    <?php if ($breadcrumb != 'none'): ?>
      <div id="breadcrumb" class="grid-<?php print $columns; ?>"><?php print $breadcrumb; ?></div>
    <?php endif; ?>    
    <?php if ($messages): ?>
      <div id="messages" class="grid-<?php print $columns; ?>"><?php print $messages; ?></div>
    <?php endif; ?>
    <?php if ($zonebanner) print $zonebanner; ?>
    <?php if ( isset ($title) && $title != 'none'): ?>
      <h1 class="title" id="page-title"><?php print $title; ?></h1>
    <?php endif; ?>
    <?php print $content; ?>
  </div>
<?php if ($wrapper): ?></div><?php endif; ?>
