<div id="content-header">       
    <div class="col span_24">
        <div id="breadcrumb" class="breadcrumb">
          <?php print $active_breadcrumb; ?>
        </div>
      <h1 class="title" id="page-title-speccol"><?php print $title; ?></h1>
    </div>
  <?php print $content; ?>
</div>