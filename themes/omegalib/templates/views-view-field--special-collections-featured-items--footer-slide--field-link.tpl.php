<?php if ($row->field_field_link): ?>
  <a href="<?php print $row->field_field_link[0]['raw']['url']; ?>" target="_blank">
    <img src="<?php print $img_path; ?>">
    <span id="tooltip">
      <div>
        <p>
            <?php
        // array element keyed 'raw' will raise lots of PHP warnings, & it does nothing to final rendering.
        // use 'rendered' only at this place, instead of its parent. 
    print render($row->field_field_caption_text[0]['rendered']);
//    print render($row->field_field_caption_text[0]['raw']);
            ?>
        </p>
      </div>
    </span>
  </a>
<?php endif; ?>
