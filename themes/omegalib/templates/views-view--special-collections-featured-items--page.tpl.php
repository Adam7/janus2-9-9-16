<div id="Fader" class="fader">
  <?php print $rows; ?>
  <div class="fader-controls">
    <button class="page prev" data-target="prev">
      <span class="glyph">&#xf053;</span>
    </button>
    <button class="page next" data-target="next">
      <span class="glyph">&#xf054;</span>
    </button>
  </div>
</div>