<article<?php print $attributes; ?>>
  <?php print $user_picture; ?>
  <?php print render($title_prefix); ?>
  <?php if (!$page && $title): ?>
  <header>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
  </header>
  <?php endif; ?>
  <?php print render($title_suffix); ?>
  <?php if ($display_submitted): ?>
  <footer class="submitted">By <?php print $author_full_name; ?> on <?php print $date_blog; ?> | <?php print $permalink; ?></footer>
  <?php endif; ?>  
  
  <div<?php print $content_attributes; ?>>
    <?php
      // We hide the comments, images and links now so that we can render them later.
      hide($content['comments']);
      hide($content['field_image_pub']);
      hide($content['links']);
      if (isset($slideshow_location)) {
        if ($slideshow_location == 'above') {
          print render($content['field_image_pub']);
        }
      }
      // print the body of the node
      print render($content);

      if (isset($slideshow_location)) {
        if ($slideshow_location == 'below') {
          print render($content['field_image_pub']);
        }
      }
    ?>
  </div>
  
  <div class="clearfix">
    <?php if (!empty($content['links'])): ?>
      <nav class="links node-links clearfix"><?php print render($content['links']); ?></nav>
    <?php endif; ?>

    <?php print render($content['comments']); ?>
  </div>
</article>
