<aside<?php print $attributes; ?>>
  <div<?php print $content_attributes; ?>>
    <?php print $content; ?>
  </div>
  <?php if (isset($feed_icons)): ?><div class="feed-icon clearfix"><?php print $feed_icons; ?></div><?php endif; ?>
</aside>
