<?php print $doctype; ?>
<html lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>"<?php print $rdf->version . $rdf->namespaces; ?>>
<head<?php print $rdf->profile; ?>>
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>
  <?php print $styles; ?>
  <link rel="stylesheet" type="text/css" href="/profiles/janus2/modules/contrib/admin_menu/admin_menu.css" />
  <link rel="stylesheet" type="text/css" href="/profiles/janus2/modules/contrib/admin_menu/admin_menu_toolbar/admin_menu_toolbar.css" />
  <style type="text/css">
    #admin-menu {font-size: 12px; }
  </style>
  <?php print $scripts; ?>
  <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body<?php print $attributes;?>>
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>
</body>
</html>