<aside class="col span_8 sidebar">
  <div id="active-tree" class="block-block">
    <h4><?php print $active_tree_title; ?></h4>
    <?php print drupal_render(menu_tree_output($active_tree_decendents)); ?>
  </div>
  <div id="quick-links" class="block-block">
    <h4>Quick Links</h4>
    <?php print $sub_menu['sub_menu_quicklinks']; ?>
  </div>
  <div class="block-block">
    <?php print $content; ?>
  </div>
</aside>
