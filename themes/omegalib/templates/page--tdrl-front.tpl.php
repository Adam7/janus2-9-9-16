<!-- Content
		============================================= -->
<section id="content">

  <div class="content-wrap">

    <div class="section header-stick bottommargin-lg clearfix" style="padding: 20px 0;">

      <div class="container clearfix">


<?php //print $messages; ?>



<br>
<h4 class="center">New Arrivals</h4>

<div class="owl-carousel image-carousel carousel-widget" data-margin="20" data-nav="true" data-pagi="false" data-items-xxs="4" data-items-xs="5" data-items-sm="6" data-items-md="8" data-items-lg="9" data-autoplay="4000" data-loop="true">

    <div class="oc-item">
        <a href="http://innopac.library.unlv.edu/record=b5365822*eng"><img src="https://www.library.unlv.edu/sites/default/files/images/pages/1_2.jpg" alt="Guardians of the Louvre" data-toggle="tooltip" title="Guardians of the Louvre" data-placement="bottom"></a>
    </div>
    <div class="oc-item">
        <a href="http://innopac.library.unlv.edu/record=b5365026*eng"><img src="https://www.library.unlv.edu/sites/default/files/images/pages/2_1.jpg" alt="Breaking Rockefeller" data-toggle="tooltip" title="Breaking Rockefeller" data-placement="bottom"></a>
    </div>
    <div class="oc-item">
        <a href="http://innopac.library.unlv.edu/record=b5365025*eng"><img src="https://www.library.unlv.edu/sites/default/files/images/pages/3_1.jpg" alt="Gorillas Up Close" data-toggle="tooltip" title="Gorillas Up Close" data-placement="bottom"></a>
    </div>
    <div class="oc-item">
        <a href="http://innopac.library.unlv.edu/record=b5365024*eng"><img src="https://www.library.unlv.edu/sites/default/files/images/pages/4_1.jpg" alt="Lucky Penny" data-toggle="tooltip" title="Lucky Penny" data-placement="bottom"></a>
    </div>
    <div class="oc-item">
        <a href="http://innopac.library.unlv.edu/record=b5365023*eng"><img src="https://www.library.unlv.edu/sites/default/files/images/pages/5_2.jpg" alt="A Girl Called Vincent" data-toggle="tooltip" title="A Girl Called Vincent" data-placement="bottom"></a>
    </div>
    <div class="oc-item">
        <a href="http://innopac.library.unlv.edu/record=b5365022*eng"><img src="https://www.library.unlv.edu/sites/default/files/images/pages/6_2.jpg" alt="Firefly Code" data-toggle="tooltip" title="Firefly Code" data-placement="bottom"></a>
    </div>
    <div class="oc-item">
        <a href="http://innopac.library.unlv.edu/record=b5364999*eng"><img src="https://www.library.unlv.edu/sites/default/files/images/pages/7.jpg" alt="Kill Shakespeare " data-toggle="tooltip" title="Kill Shakespeare" data-placement="bottom"></a>
    </div>
    <div class="oc-item">
        <a href="http://innopac.library.unlv.edu/record=b5364998*eng"><img src="https://www.library.unlv.edu/sites/default/files/images/pages/8.jpg" alt="Kill Shakespeare" data-toggle="tooltip" title="Kill Shakespeare" data-placement="bottom"></a>
    </div>
    <div class="oc-item">
        <a href="http://innopac.library.unlv.edu/record=b5364918*eng"><img src="https://www.library.unlv.edu/sites/default/files/images/pages/9.jpg" alt="Remix" data-toggle="tooltip" title="Remix" data-placement="bottom"></a>
    </div>
    <div class="oc-item">
        <a href="http://innopac.library.unlv.edu/record=b5364917*eng"><img src="https://www.library.unlv.edu/sites/default/files/images/pages/10.jpg" alt="Lucky Strikes" data-toggle="tooltip" title="Lucky Strikes" data-placement="bottom"></a>
    </div>
    <div class="oc-item">
        <a href="http://innopac.library.unlv.edu/record=b5364916*eng"><img src="https://www.library.unlv.edu/sites/default/files/images/pages/11.jpg" alt="Soar" data-toggle="tooltip" title="Soar" data-placement="bottom"></a>
    </div>
    <div class="oc-item">
        <a href="http://innopac.library.unlv.edu/record=b5364651*eng"><img src="https://www.library.unlv.edu/sites/default/files/images/pages/12.jpg" alt="Kill Shakespeare V2" data-toggle="tooltip" title="Kill Shakespeare V2" data-placement="bottom"></a>
    </div>
</div>

    
                        </div>
                    
                </div> 
                
                
                

                
                

                <div class="container clearfix">

                    <div class="row">

                        <div class="col-md-6 bottommargin">

                            <div class="col_full bottommargin-lg clearfix">

                                <div class="fancy-title title-border">
                                    <h3>News</h3>
                                </div>

                                                                <div class="ipost clearfix">
                                    <div class="col_one_third bottommargin-sm">
                                        <div class="entry-image">
                                            <a href="https://www.library.unlv.edu/intrepid_reader"><img class="image_fade" src="https://www.library.unlv.edu/sites/default/files/images/pages/blog1.jpg" alt="Blog 1"></a>
                                        </div>
                                    </div>
                                    <div class="col_two_third col_last bottommargin-sm col_last">
                                        <div class="entry-title">
                                            <h3><a href="https://www.library.unlv.edu/intrepid_reader">Intrepid Reader: Tucky Jo and Little Heart by Patricia Polacco</a></h3>
                                        </div>
                                        <ul class="entry-meta clearfix">
                                            <li><i class="icon-calendar3"></i> 26th Aug 2016</li>
                                            <li><a href="https://www.library.unlv.edu/intrepid_reader"><i class="icon-comments"></i> 21</a></li>
                                            <li><a href="https://www.library.unlv.edu/intrepid_reader"><i class="icon-book3"></i></a></li>
                                        </ul>
                                        <div class="entry-content">
                                            <p>Friendship, loyalty, and kindness stand the test of time in this heartwarming World War II-era picture book based on a true story from the beloved author-illustrator of Pink and Say and The Keeping Quilt . Tucky Jo was known as the "kid from Kentucky" when he enlisted in the army at age fifteen. Being the youngest recruit in the Pacific ...</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="clear"></div>

                                

                                <div class=" col_full nobottommargin col_last">

                                    <div class="spost clearfix">
                                        <div class="entry-image">
                                            <a href="https://www.library.unlv.edu/intrepid_reader"><img src="https://www.library.unlv.edu/sites/default/files/images/pages/blog2.jpg" alt="blog 2"></a>
                                        </div>
                                        <div class="entry-c">
                                            <div class="entry-title">
                                                <h4><a href="https://www.library.unlv.edu/intrepid_reader">Intrepid Reader: Personal Effects</a></h4>
                                            </div>
                                            <ul class="entry-meta">
                                                <li><i class="icon-calendar3"></i> 11th Jun 2016</li>
                                                <li><a href="https://www.library.unlv.edu/intrepid_reader"><i class="icon-comments"></i> 7</a></li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="spost clearfix">
                                        <div class="entry-image">
                                            <a href="https://www.library.unlv.edu/intrepid_reader"><img src="https://www.library.unlv.edu/sites/default/files/images/pages/blog3.jpg" alt=""></a>
                                        </div>
                                        <div class="entry-c">
                                            <div class="entry-title">
                                                <h4><a href="https://www.library.unlv.edu/intrepid_reader">Intrepid Reader: Illuminae</a></h4>
                                            </div>
                                            <ul class="entry-meta">
                                                <li><i class="icon-calendar3"></i> 17th May 2016</li>
                                                <li><a href="https://www.library.unlv.edu/intrepid_reader"><i class="icon-comments"></i> 14</a></li>
                                            </ul>
                                        </div>
                                    </div>

                                </div>

                            </div>

                            

                            

                            
                            <!-- facebook widget -->
                            <div class="widget clearfix">
                                    <iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2FUNLVTeacherDevelopmentResourcesLibrary&amp;width=360&amp;height=240&amp;colorscheme=light&amp;show_faces=true&amp;header=true&amp;stream=false&amp;show_border=true&amp;appId=499481203443583" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:360px; height:240px; max-width: 100% !important;" allowTransparency="true"></iframe>
                            </div>
                            <!-- end facebook widget -->

                        </div>




        <!-- Second Column Start -->
        <div class="col-md-6 col-sm-12 no-gutter">

          <div class="line hidden-lg hidden-md"></div>








          <div class="fancy-title title-border">
            <h3>FAQS</h3>
          </div>

          <div class="col-md-7">
            <div class="r-margin">
              <div class="accordion clearfix" data-active="1">

                <div class="acctitle"><i class="acc-closed icon-ok-circle"></i><i class="acc-open icon-remove-circle"></i>How do I view articles from home or off-campus?</div>
                <div class="acc_content clearfix">When prompted, use either your <a href="http://oit.unlv.edu/accounts/active-directory">ACE account</a> OR your <a href="https://www.library.unlv.edu/help/activating_your_account">activated RebelCard barcode and PIN </a>to login.
                  <br> <a href="http://ask.library.unlv.edu/faq/40676">More <i class="icon-line-arrow-right"></i></a></div>

                <div class="acctitle"><i class="acc-closed icon-ok-circle"></i><i class="acc-open icon-remove-circle"></i>How do I find peer reviewed or scholarly articles?</div>
                <div class="acc_content clearfix"><a href="http://ask.library.unlv.edu/faq/42921">This tutorial </a>is available to help you! Additional help is available if you need to <a href="http://ask.library.unlv.edu/">access journal article from off-campus.</a></div>

                <div class="acctitle"><i class="acc-closed icon-ok-circle"></i><i class="acc-open icon-remove-circle"></i>Where can I get help with my ACE Account?</div>
                <div class="acc_content clearfix">For help with your ACE account, RebelMail, WebCampus or Secure Wireless, please contact the campus OIT help desk at 702-895-0777 or visit them at CBC B113 (during regular hours).
                  <br><a href="http://ask.library.unlv.edu/faq/56515">More <i class="icon-line-arrow-right"></i></a></div>

              </div>

            </div>
          </div>

          <div class="col-md-5 widget_links bottommargin-sm">


            <h4>Library Guides:</h4>
            <ul>
              <li><a href="http://guides.library.unlv.edu/education">Education Guide</a></li>
              <li><a href="http://guides.library.unlv.edu/cyalit">Children's and Young Adult Literature Guide</a></li>
              <li><a href="http://guides.library.unlv.edu/tests">Tests and Measures Guide </a></li>
              <li><a href="http://guides.library.unlv.edu/coe102">COE 102/202 Guide</a></li>
            </ul>


          </div>


          <br>



          <!-- Slider -->
                            
                            <div class="col-md-12">
								<div class="fslider flex-thumb-grid grid-4" data-animation="fade" data-arrows="true" data-thumbs="true">
									<div class="flexslider">
										<div class="slider-wrap">
											<div class="slide" data-thumb="https://www.library.unlv.edu/sites/default/files/images/pages/t1.jpg">
												<a href="#">
													<img src="https://www.library.unlv.edu/sites/default/files/images/pages/s1.jpg" alt="tdrl">
													<div class="overlay">
														<div class="text-overlay">
															<div class="text-overlay-title">
																<h3></h3>
															</div>
															<div class="text-overlay-meta">
																<span></span>
															</div>
														</div>
													</div>
												</a>
											</div>
											<div class="slide" data-thumb="https://www.library.unlv.edu/sites/default/files/images/pages/t2.jpg">
												<a href="#">
													<img src="https://www.library.unlv.edu/sites/default/files/images/pages/s2.jpg" alt="tdrl">
													<div class="overlay">
														<div class="text-overlay">
															<div class="text-overlay-title">
																<h3></h3>
															</div>
															<div class="text-overlay-meta">
																<span></span>
															</div>
														</div>
													</div>
												</a>
											</div>
											<div class="slide" data-thumb="https://www.library.unlv.edu/sites/default/files/images/pages/t3.jpg">
												<a href="#">
													<img src="https://www.library.unlv.edu/sites/default/files/images/pages/s3.jpg" alt="tdrl">
													<div class="overlay">
														<div class="text-overlay">
															<div class="text-overlay-title">
																<h3></h3>
															</div>
															<div class="text-overlay-meta">
																<span></span>
															</div>
														</div>
													</div>
												</a>
											</div>
											
											<div class="slide" data-thumb="https://www.library.unlv.edu/sites/default/files/images/pages/t4.jpg">
												<a href="#">
													<img src="https://www.library.unlv.edu/sites/default/files/images/pages/s4.jpg" alt="tdrl">
													<div class="overlay">
														<div class="text-overlay">
															<div class="text-overlay-title">
																<h3></h3>
															</div>
															<div class="text-overlay-meta">
																<span></span>
															</div>
														</div>
													</div>
												</a>
											</div>
											
										</div>
									</div>
								</div>
							</div>
                            
                            <!-- Slider End -->







        </div>

      </div>

    </div>

  </div>

</section>
<!-- #content end -->
