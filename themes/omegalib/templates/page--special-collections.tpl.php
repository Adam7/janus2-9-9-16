  <section class="content">
    <div class="container">
      <div class="row">
        <div class="col span_24">
          <?php print render($page['header']); ?>
        </div>
      </div>
      <div class="row">
        <?php print render($page['content']); ?>
      </div>
    </div>
  </section>