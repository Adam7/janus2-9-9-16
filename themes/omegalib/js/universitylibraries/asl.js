(function($){
  $(document).ready(function(){
    // Global: stuff for responsive site
    function automagicResize() {
      // h = $(window).height();
      if ($('.mobilecheck').css('z-index') != 1 ) {
        $('nav.main-nav ul#main-menu').show();
        // $('header').css({'height': ((.382 * h)-$('#global-header-bar').height()) + 'px'});
      } else {
        $('nav.main-nav ul#main-menu').hide();
        // $('section.header').css({'height': 'auto'});
      }
      // Special Collections: responsive slideshow
      if ($('.fader')) {
        var slideImgHeight = -1;
        $('.fader .slide img').each(function() {
          if ($(this).height() > slideImgHeight)
            slideImgHeight = $(this).height();
        });
        $('.fader').css({
          'padding-top': slideImgHeight + 54 + 'px'
        });
      }
    }

    // Global: parallax scrolling effect for header background image
    function parallax() {
      $(window).scroll(function() {
        var scrolledY = $(window).scrollTop();
        $('div.page.header').css('background-position', 'left ' + (scrolledY*.5) + 'px');
      });
    }

    // Global: mobile menu button that shows and hides main navigation
    function mobileMenu() {
      $('nav .mobile-menu button').on('click', function() {
        $('nav.main-nav ul#main-menu').slideToggle(200);
      });
    }

    // Global: Select2
    // function select2() {
    //   $("#liblocation").select2({
    //     minimumResultsForSearch: -1,
    //     width: 200
    //   });
    //   $("select").select2({
    //     minimumResultsForSearch: -1
    //   });
    // }

    // Global: add home glyph to main menu
    function replaceHomeWithHouseIcon() {
      $('ul#main-navigation li a.first.home').html('<span class="glyph">&#xf015;</span>');
      // and add actve class span to current menu item
      $('ul#main-navigation a').after('<span class="arrow-active"></span>');
    }

    // Architecture Studies Library: moves left sidebar contents over to right sidebar on tablet resolution
    // moves left sidebar contents over to right sidebar on tablet resolution
    function tabletSecondSidebar() {
      if ($(window).width() < 960) {
        $('#home .wrapper').removeClass('span_18').addClass('span_16');
        $('#second-sidebar .moveable').appendTo('#main-sidebar').show('scale');
        $('#second-sidebar').insertAfter($('#main-sidebar')).hide();
        $('#home-content').removeClass('span_16').addClass('span_24');
        $('#main-sidebar').removeClass('span_6').addClass('span_8');
      } else {
        $('#home .wrapper').removeClass('span_16').addClass('span_18');
        $('#second-sidebar').insertBefore($('#home-content')).show();
        $('#main-sidebar .moveable').appendTo('#second-sidebar');
        $('#home-content').removeClass('span_24').addClass('span_16');
        $('#main-sidebar').removeClass('span_8').addClass('span_6');
      }
    };

    // Architecture Studies Library: Masonry for Pinterest feed
    function masonryInitialize() {
      console.log("bleh");
      var container = $('#pinterest');
      // initialize
      container.masonry({
        // options
        gutter: 4,
        columnWidth: '.grid-sizer',
        itemSelector: '.item'
      });
    }

    // $(document).ready(function(){
      replaceHomeWithHouseIcon();
      automagicResize();
      parallax();
      // styledRadios();
      // select2();
      mobileMenu();
      tabletSecondSidebar();
    // });

    $(window).resize(function(){
      automagicResize();
      tabletSecondSidebar();
    });
    
    masonryInitialize();

    // footer branch switcher stuff
    $('#liblocation').focus(function(e) {
      prev = this.value;
    }).change(function(e) {
      // remove active class from current branch shown
      $('.lib-branch.active').removeClass('active');
      // add active call to selected branch
      $('#lib-'+$(this).val()).addClass('active');
      // swap map by removing old class and adding new one
      $('.map').removeClass('map-'+prev);
      $('.map').addClass('map-'+$(this).val());
      // swap today's hours
      if ($(this).val() == 'speccol') {
        $('#lib-specialcol-hours').removeClass('active');
        $('#lib-specialcol-hours').addClass('active');
      } else {
        $('#lib-'+prev+'-hours').removeClass('active');
        $('#lib-'+$(this).val()+'-hours').addClass('active');
      }
      if ($(this).val() == 'law') {
        $('h5.todays-hours').hide();
      } else {
        $('h5.todays-hours').show();
      }
      // update prev to current incase they don't blur the selectbox
      prev = $(this).val();
    });
  });
})(jQuery);
