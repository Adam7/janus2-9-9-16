(function($) {

  // Global: stuff for responsive site
  function automagicResize() {
    // h = $(window).height();
    if ($('.mobilecheck').css('z-index') != 1 ) {
      $('nav.main-nav ul#main-menu').show();
      // $('header').css({'height': ((.382 * h)-$('#global-header-bar').height()) + 'px'});
    } else {
      $('nav.main-nav ul#main-menu').hide();
      // $('section.header').css({'height': 'auto'});
    }
    // Special Collections: responsive slideshow
    if ($('.fader')) {
      var slideImgHeight = -1;
      $('.fader .slide img').each(function() {
        if ($(this).height() > slideImgHeight)
          slideImgHeight = $(this).height();
      });
      $('.fader').css({
        'padding-top': slideImgHeight + 54 + 'px'
      });
    }
  }

  // Global: parallax scrolling effect for header background image
  function parallax() {
    $(window).scroll(function() {
      var scrolledY = $(window).scrollTop();
      $('section.header').css('background-position', 'left ' + (scrolledY*.5) + 'px');
    });
  }

  // Global: mobile menu button that shows and hides main navigation
  function mobileMenu() {
    $('nav .mobile-menu button').on('click', function() {
      $('nav.main-nav ul#main-menu').slideToggle(200);
    });
  }

  // Global: Select2
  function select2() {
    $("#liblocation").select2({
      minimumResultsForSearch: -1,
      width: 200
    });
    $("select").select2({
      minimumResultsForSearch: -1
    });
  }

  // Global: add home glyph to main menu
  function replaceHomeWithHouseIcon() {
    $('ul#main-menu li a.first.home').html('<span class="glyph">&#xf015;</span>');
  }

  // Special Collections: search tool
  function speccolSearch() {
    $('#sc-search input:radio').on('click', function(event) {
      $('#sc-search form').attr('data-active', 'inactive');
      $('#sc-search form#' + event.target.value ).attr('data-active', 'active');
    });
  }

  // Special Collections: image carousel before footer
  function imageCarousel() {
    if( $('div.img-carousel') ) {
      var begin = 'div.img-carousel div.images a:first-child';
      var end = 'div.img-carousel div.images a:last-child';
      // 'false' is required for both outerHeight and outerWidth as jQueryUI 1.8.11 breaks them without it.  This is fixed in jQueryUI 1.8.22.
      $('span.paddle').css({'line-height': $('span.paddle').outerHeight(false) + 'px'});
      $('span.paddle').on('click', function(event){
        if( $(event.target).hasClass('right') ){
          if( $(window).width() - ($(end).offset().left + $(end).outerWidth(false)) < 0 ) {
            $('div.img-carousel div.images').animate({'margin-left': '-=' + $(begin).outerWidth(false) + 'px'}, 200);
          }
        } else {
          if( $(begin).offset().left <= 0 ) {
            $('div.img-carousel div.images').animate({'margin-left': '+=' + $(begin).outerWidth(false) + 'px'}, 200);
          }
        }
      });
    }
  }

  $(document).ready(function() {
    parallax();
    mobileMenu();
    automagicResize();
    speccolSearch();
    imageCarousel();
    select2();
    replaceHomeWithHouseIcon();
  });

  $(window).resize(function(){
    automagicResize();
  });
 
})(jQuery);  