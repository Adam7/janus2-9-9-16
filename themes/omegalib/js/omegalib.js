(function($){
	$(document).ready(function(){

    // build an awesome tag cloud
    function rowobject(param1, param2){
      this.name=param1;
      this.countresult=param2;
    }
    // create array that will be filled with objects
    var rows = Array();
    // grab a tag count for each tag
    $.each($('div.view-id-blog_content.view-display-id-block_4 div.views-row-last'), function() {
      var rowcount = $(this).attr('class').replace(/\D/g,'');
      // put tags into an array as objects
      var row=new rowobject($(this).html(),rowcount);
      rows.push(row);
    });
    //sort by count, descending
    rows.sort(function(a, b) {
      return b.countresult-a.countresult;
    });
    // print the top 20 tags
    var i = 0;
    while (i < 20) {
      if (rows[i]) { // added if statement to check if rows array value exists --JM 01/15/2014
        var currentRowSplit = rows[i].name.split("field-content");
        // add rank class
        rows[i].name = currentRowSplit[0]+"field-content final rank-"+i+currentRowSplit[1];
        $('div.view-id-blog_content.view-display-id-block_4').append(rows[i].name);
      }
      i++;
    }
    // remove all old elements
    $('div.view-id-blog_content.view-display-id-block_4 div.grouped-tag').remove();

    // handle giving events slideshow iframe width (temp hack 3/14/2014)
    iframefit();
    $(window).resize(function(event) {
      iframefit();
    });
    function iframefit() {
      if ($('div#region-content').width() < 620) {
        $('iframe').width(460);
      } else {
        $('iframe').width(620);
      }
    }
    
  });
})(jQuery);