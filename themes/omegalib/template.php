<?php

/**
 * Implements hook_process_zone().
 */
function omegalib_alpha_process_zone(&$vars) {
    $theme = alpha_get_theme();

    if ($vars['elements']['#zone'] == 'content')
    {
        // check if the page is a term page that belongs to either faq categories or tags
        if (arg(0) == 'taxonomy')
        {
            $tax_term = taxonomy_term_load(arg(2));

            if ( is_object ($tax_term) )
            {
                $tmp_name = $tax_term->vocabulary_machine_name;

                if ( 'faq_categories' == $tmp_name || 'faq_tags' == $tmp_name )
                {
                    // override breadcrumbs on faq
                    $vars['breadcrumb'] = '<div class="breadcrumb"><a href="'. base_path() .'" data-bitly-type="bitly_hover_card">Home</a><span> > </span><a href="'. base_path() .'help">Get Help</a></div>';
                    // put "Get Help - " before the title
                    $vars['title'] = "Get Help - " . $theme->page['title'];
                }
                else
                    $vars['title'] = $theme->page['title'];
            }
        }
        else
            $vars['title'] = $theme->page['title'];
    }
}

/**
 * Implements hook_preprocess_html().
 */
function omegalib_preprocess_html(&$vars) {
  // preprocess the user link image in footer zed
  // load user object
  global $user;
  // provide default path & text by adding it to the user object
  $user->link_image_path = drupal_get_path('module', 'library').'/assets/user.png';
  $user->link_image_text = t('User Login');
  // provide path for image and associated text when logged in
  if ($user->uid > 0) {
    $user->link_image_path = drupal_get_path('module', 'library').'/assets/user-active.png';
    $user->link_image_text = 'User Page';
  }
  // prepare options for link; add user class and allow html
  $options['attributes']['class'][] = 'user';
  $options['html'] = TRUE;
  // prepare destination after login
  $options['query']['destination'] = drupal_get_path_alias();
  // return themed output of the user image link
  $vars['user_link'] =  theme(
    'library_footer_zed',
    array(
      'user_link' => l(
        theme(
          'image',
          array(
            'alt' => $user->link_image_text,
            'path' => drupal_get_path(
              'module',
              'library_content'
            ).$user->link_image_path,
            'title' => $user->link_image_text,
          )
        ),
        'user',
        $options
      )
    )
  );
}

/**
 * Implements hook_preprocess_block().
 *
 * This is being used mainly to hide block titles of library_content by default.
 */
function omegalib_preprocess_block(&$vars, $hook) {
  // hide block titles of blocks in the library_content & libsearch modules
  if ($vars['block']->module == 'library_content' || $vars['block']->module == 'libsearch') {
    $vars['elements']['#block']->subject =NULL;
  }
}

function omegalib_preprocess_views_view_row_rss(&$vars) {
  // provide row level info about nodes to the rss feed template
  $view = &$vars['view'];
  $options = &$vars['options'];
  $item = &$vars['row'];
  // Use the [id] of the returned results to determine the nid in [results]
  $result = &$vars['view']->result;
  $id  = &$vars['id'];
  $node  = node_load( $result[$id-1]->nid );
  $vars['node'] = $node;
  // provide a string of tags on nodes seperated by commas
  if (isset($node->field_tags)) {
    if (array_key_exists('und', $node->field_tags)) {
      foreach ($node->field_tags['und'] as $tag) {
        $tags[] = taxonomy_term_load($tag['tid']);
      }
    }
  }
  if (isset($tags)) {
    foreach ($tags as $tag) {
      $vars['tags'][] = $tag->name;
    }
  }
}
function omegalib_preprocess_views_view_summary(&$vars) {
  // inject a slash in the monthly archives links for the blogs
  if ($vars['view']->name == 'blog_content' && $vars['view']->current_display == 'block_2') {
    foreach ($vars['rows'] as $row) {
      $row->url = substr_replace($row->url, '/', -2, 0) ;
    }
  }
}

function omegalib_preprocess_views_view_field(&$vars) {
  // provide path to image for "Collection Strength" footer slide for speccol
  if (property_exists($vars['field'], 'field_info')) {
    switch ($vars['field']->field_info['field_name']) {
      case 'field_link':
        if ($vars['field']->view->current_display == 'footer_slide') {
          $vars['img_path'] = file_create_url($vars['row']->field_field_image_pub[0]['raw']['uri']);
        }
        break;
      default:
        break;
    }
  }
}

/**
 * Implements hook_theme().
 */
function omegalib_theme() {
  return array(
    'zonebanner_econnections' => array(
      'template' => 'templates/omegalib--zonebanner-econnections',
    ),
    'zonebanner_btl' => array(
      'template' => 'templates/omegalib--zonebanner-btl',
    ),
    'zonebanner_linkeddata' => array(
      'template' => 'templates/omegalib--zonebanner-linkeddata',
    ),
  );
}

/**
 * Implements theme_breadcrumb().
 */
function omegalib_breadcrumb ($vars) {
  // set breadcrumbs for blog (landing/view) pages
  if (arg(0) == 'taxonomy') {
    $term = taxonomy_term_load(arg(2));

    if ( is_object ($term) )
    {
        if ($term->vocabulary_machine_name == 'blog_name')
            $vars['breadcrumb'][] = l('Blogs', 'blogs');
    }
  }
  // set breadcrumbs for blog post pages
  if (arg(0) == 'node'){
    $node = node_load(arg(1));
    if ($node->type == 'blog') {
      $term = taxonomy_term_load($node->field_blog_name['und'][0]['tid']);
      unset($vars['breadcrumb']);
      $vars['breadcrumb'][] = l('Home', '<front>');
      $vars['breadcrumb'][] = l('Blogs', 'blogs');

        if ( is_object ($term) )
            $vars['breadcrumb'][] = l($term->name, drupal_get_path_alias('taxonomy/term/'.$term->tid));
    }
  };
  // Remove breadcrumb from econnections & btl page
  if ((arg(0) == 'taxonomy' && arg(2) == '1066') || (arg(0) == 'taxonomy' && arg(2) == '1065')) {
    $vars['breadcrumb'] = NULL;
  }
  return theme_breadcrumb($vars);
}

/**
 * Implements theme_feed_icon().
 */
function omegalib_feed_icon($vars) {
  $text = t('Subscribe to !feed-title', array('!feed-title' => $vars['title']));
  if ($image = theme('image', array('path' => 'misc/feed.png', 'width' => 16, 'height' => 16, 'alt' => $text))) {
    return l($image."<span>Subscribe to this blog's feed</span>", $vars['url'], array('html' => TRUE,
      'attributes' => array('class' => array('feed-icon'),
      'title' => $text)));
  }
}

/**
 * Implements theme_links().
 */
function omegalib_link($vars) {
  // inject the current blog into the path of the linked category
  if (array_key_exists('entity', $vars['options'])) {
    if (array_key_exists('vocabulary_machine_name', $vars['options']['entity'])) {
      if ($vars['options']['entity']->vocabulary_machine_name == 'categories_blog' || $vars['options']['entity']->vocabulary_machine_name == 'tags') {
        if (arg(0) == 'node') {
          $vars['path'] = 'taxonomy/term/'.node_load(arg(1))->field_blog_name['und'][0]['tid'].'/'.$vars['options']['entity']->tid;
        }
        // TODO: change the links on blog landing view pages
      }
    }
  }
  // add arch pinterest fontawesome icon to link (in theme, facebook link is done in module for show)
  if ( isset ($vars['options']['attributes']['class'][0])
       && ($vars['options']['attributes']['class'][0] == 'pinterest-circle') ) {
    $vars['options']['html'] = TRUE;
    $vars['text'] = '<i class="fa fa-pinterest"></i>'. $vars['text'];
  }

    // Do not urlencode external link when going through ezproxy. Take the link as is.
    if ( FALSE !== strpos ($vars['path'], 'ezproxy.library.unlv.edu') )
    {
        $url = $vars['path'] . "?url=" . $vars['options']['query']['url'];
        return '<a href="' . check_plain($url) . '"' . drupal_attributes($vars['options']['attributes']) . ' target="_blank">' . ($vars['options']['html'] ? $vars['text'] : check_plain($vars['text'])) . '</a>';
    }

  return theme_link($vars);
}

/**
 * Implements HOOK_form_comment_form_alter().
 *   or rather HOOK_form_FORM_ID_alter().
 */
function omegalib_form_comment_form_alter(&$form) {
  // remove 'homepage' field from comment form
  $form['author']['homepage']['#access'] = FALSE;
  $form['actions']['submit']['#value'] = 'submit';
}
