	<h2 class="hack-magic">Contact</h2>
	<div class="grid-6 alpha col">
	  <div class="footer-block">
	    <h5>
	      Ask Us!
	    </h5>
	    <ul class="no-bullet">
	      <li>
	        <a href="#" onclick="Chat=window.open('https://v2.libanswers.com/widget_standalone.php?hash=2de07cac2d72945bcdf47593b1c7308d','Chat','toolbar=no,location=no,directories=no,status, menubar=no,scrollbars,resizable,width=350,height=700'); return false;">
	          <span class="glyph">&#xf086;</span> Chat
	        </a>
	      </li>
	      <li>
	        <span class="footer-text">
	          <span class="glyph">&#xf10a;</span> Text: (702) 945-0822
	        </span>
	      </li>
	      <li>
	        <span class="footer-text">
	          <span class="glyph">&#xf095;</span> Phone: (702) 895-2111
	        </span>
	      </li>
	      <li>
	        <a href="/ask/email.php">
	          <span class="glyph">&#xf0e0;</span> Email
	        </a>
	      </li>
	    </ul>
	  </div>
	  <div class="footer-block">
	    <h5>
	      Social Media
	    </h5>
	    <div class="contact-group-links ">
	      <ul class="no-bullet">
	        <li>
	          <a href="https://www.facebook.com/unlvlib">
	            <span class="glyph facebook">&#xf082;</span> Facebook
	          </a>
	        </li>
	        <li>
	          <a href="https://twitter.com/unlvlibraries">
	            <span class="glyph twitter">&#xf099;</span> Twitter
	          </a>
	        </li>
	        <li>
	          <a href="http://www.youtube.com/user/unlvlibraries">
	            <span class="glyph youtube">&#xf04b;</span> YouTube
	          </a>
	        </li>        <li>
	          <a href="http://instagram.com/unlvlibraries">
	            <span class="glyph instagram">&#xf16d;</span> Instagram
	          </a>
	        </li>
	        <li>
	            <a href="/contact/social_media">All Social Media</a>
	        </li>
	      </ul>
	    </div>
	  </div>
	</div>
	<div class="grid-6 omega col">
	  <div class="footer-block">
	    <h5>
	      Directories
	    </h5>
	    <ul class="no-bullet">
	      <li>
	        <a href="/contact/librarians_by_subject">Librarians by Subject</a>
	      </li>
	      <li>
	        <a href="/about/staff/libstafinfo.php?style=2">Staff Directory</a>
	      </li>
	    </ul>
	  </div>
	  <div class="footer-block">
	    <h5>
	      Employment
	    </h5>
	    <ul class="no-bullet">
	      <li>
	        <a href="/employment">Full-Time</a>
	      </li>
	      <li>
	        <a href="/employment/student.html">Student</a>
	      </li>
	    </ul>
	  </div>
	  <div class="footer-block">
	    <h5>
	      Feedback
	    </h5>
	      <ul class="no-bullet">
	        <li>
	          <a href="/comments.php"><span class="glyph">&#xf0a1;</span> Provide Feedback</a>
	        </li>
	    </ul>
	  </div>
	</div>
