<?php
/**
 * @file
 * library_content.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function library_content_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "delta" && $api == "delta") {
    return array("version" => "3");
  }
  if ($module == "flexslider" && $api == "flexslider_default_preset") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function library_content_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_eck_bundle_info().
 */
function library_content_eck_bundle_info() {
  $items = array(
  'lib_featured_item_lib_featured_item' => array(
  'machine_name' => 'lib_featured_item_lib_featured_item',
  'entity_type' => 'lib_featured_item',
  'name' => 'lib_featured_item',
  'label' => 'lib_featured_item',
),
  );
  return $items;
}

/**
 * Implements hook_eck_entity_type_info().
 */
function library_content_eck_entity_type_info() {
$items = array(
       'lib_featured_item' => array(
  'name' => 'lib_featured_item',
  'label' => 'lib_featured_item',
  'properties' => array(
    'uid' => array(
      'label' => 'Author',
      'type' => 'integer',
      'behavior' => 'author',
    ),
    'created' => array(
      'label' => 'Created',
      'type' => 'integer',
      'behavior' => 'created',
    ),
    'changed' => array(
      'label' => 'Changed',
      'type' => 'integer',
      'behavior' => 'changed',
    ),
    'title' => array(
      'label' => 'Title',
      'type' => 'text',
      'behavior' => 'title',
    ),
  ),
),
  );
  return $items;
}

/**
 * Implements hook_image_default_styles().
 */
function library_content_image_default_styles() {
  $styles = array();

  // Exported image style: featured_max_width.
  $styles['featured_max_width'] = array(
    'effects' => array(
      4 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 620,
          'height' => 349,
        ),
        'weight' => 2,
      ),
    ),
    'label' => 'featured_max_width',
  );

  // Exported image style: library_news.
  $styles['library_news'] = array(
    'label' => 'library_news',
    'effects' => array(
      6 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 220,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: page_insert.
  $styles['page_insert'] = array(
    'effects' => array(
      2 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 560,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
    'label' => 'page_insert',
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function library_content_node_info() {
  $items = array(
    'page' => array(
      'name' => t('Page'),
      'base' => 'node_content',
      'description' => t('used for general page usage'),
      'has_title' => '1',
      'title_label' => t('Page Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
