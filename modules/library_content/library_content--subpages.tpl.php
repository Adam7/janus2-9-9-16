<h2><?php print $section; ?> Links</h2>
<?php if ($subpages): ?>
  <?php $tmp_tree = menu_tree_output($subpages); print drupal_render($tmp_tree); ?>
<?php elseif ($siblings): ?>
  <?php $tmp_tree = menu_tree_output($siblings); print drupal_render($tmp_tree); ?>
<?php endif; ?>
