(function ($) {
  $(document).ready(function() {
    // write today's hours in the branding section
    $('span#lied-todays-hours').text($('div#lib-lied-hours').text());
    $('#library-select').change(function() {
      // hide info & remove active class from all options
      $('div.lib-branch').hide().removeClass('active');
      newLib = "#lib-" + $(this).val();
      newLibHours = "#lib-" + $(this).val() + "-hours";
      // fade in new lib info
      $(newLib).fadeIn().addClass('active');
      $(newLibHours).fadeIn().addClass('active');
      // if law lib is selected hide hours
      if($(this).val() == 'law') {
        $('div.col.hours h5').hide();
      } else {
        $('div.col.hours h5').show();
      }
    });
  });
})(jQuery);
