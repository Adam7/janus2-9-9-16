<?php
/**
 * @file
 * library_content.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function library_content_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'arch';
  $context->description = '';
  $context->tag = 'Architecture Studies Library';
  $context->conditions = array(
    'node_taxonomy' => array(
      'values' => array(
        1193 => 1193,
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
    'path' => array(
      'values' => array(
        'arch' => 'arch',
        'arch2' => 'arch2',
        'arch/*' => 'arch/*',
        'arch2/*' => 'arch2/*',
        'architecture-studies-library' => 'architecture-studies-library',
        'architecture-studies-library/*' => 'architecture-studies-library/*',
      ),
    ),
  );
  $context->reactions = array(
    'delta' => array(
      'delta_template' => 'architecture_studies_library',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Architecture Studies Library');
  $export['arch'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'arch_front';
  $context->description = '';
  $context->tag = 'Architecture Studies Library';
  $context->conditions = array(
    'node_taxonomy' => array(
      'values' => array(
        1193 => 1193,
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
    'path' => array(
      'values' => array(
        'arch' => 'arch',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-c3ffda3459cd8c1a31b1718a04639a66' => array(
          'module' => 'views',
          'delta' => 'c3ffda3459cd8c1a31b1718a04639a66',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
      ),
    ),
    'delta' => array(
      'delta_template' => 'architecture_studies_library_fro',
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Architecture Studies Library');
  $export['arch_front'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'blog_econnection';
  $context->description = '';
  $context->tag = 'blogs';
  $context->conditions = array(
    'node_taxonomy' => array(
      'values' => array(
        1066 => 1066,
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
    'path' => array(
      'values' => array(
        'econnections' => 'econnections',
        'econnections/*' => 'econnections/*',
        '~econnections/feedback' => '~econnections/feedback',
        'taxonomy/term/1066/*' => 'taxonomy/term/1066/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'library_content-econ_social' => array(
          'module' => 'library_content',
          'delta' => 'econ_social',
          'region' => 'sidebar_second',
          'weight' => '-52',
        ),
        'webform-client-block-19157' => array(
          'module' => 'webform',
          'delta' => 'client-block-19157',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('blogs');
  $export['blog_econnection'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'blog_gaming';
  $context->description = '';
  $context->tag = 'blogs';
  $context->conditions = array(
    'node_taxonomy' => array(
      'values' => array(
        37 => 37,
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
    'path' => array(
      'values' => array(
        'center_for_gaming_research' => 'center_for_gaming_research',
        'center_for_gaming_research/*' => 'center_for_gaming_research/*',
        'taxonomy/term/37' => 'taxonomy/term/37',
        'taxonomy/term/37/*' => 'taxonomy/term/37/*',
      ),
    ),
  );
  $context->reactions = array(
    'delta' => array(
      'delta_template' => 'gaming_blog',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('blogs');
  $export['blog_gaming'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'blogs';
  $context->description = '';
  $context->tag = 'blogs';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'blog' => 'blog',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
    'taxonomy_term' => array(
      'values' => array(
        'blog_name' => 'blog_name',
        'categories_blog' => 'categories_blog',
        'tags' => 'tags',
      ),
      'options' => array(
        'term_form' => '0',
      ),
    ),
    'views' => array(
      'values' => array(
        'blog_content:page' => 'blog_content:page',
        'blog_content:page_1' => 'blog_content:page_1',
        'blog_content:page_2' => 'blog_content:page_2',
        'taxonomy_term:page_1' => 'taxonomy_term:page_1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'system-main' => array(
          'module' => 'system',
          'delta' => 'main',
          'region' => 'content',
          'weight' => '-10',
        ),
        'views--exp-blog_content-page' => array(
          'module' => 'views',
          'delta' => '-exp-blog_content-page',
          'region' => 'sidebar_second',
          'weight' => '-50',
        ),
        'views-blog_content-block_3' => array(
          'module' => 'views',
          'delta' => 'blog_content-block_3',
          'region' => 'sidebar_second',
          'weight' => '-49',
        ),
        'views-blog_content-block_2' => array(
          'module' => 'views',
          'delta' => 'blog_content-block_2',
          'region' => 'sidebar_second',
          'weight' => '-48',
        ),
        'views-blog_content-block_1' => array(
          'module' => 'views',
          'delta' => 'blog_content-block_1',
          'region' => 'sidebar_second',
          'weight' => '-47',
        ),
        'views-blog_content-block_4' => array(
          'module' => 'views',
          'delta' => 'blog_content-block_4',
          'region' => 'sidebar_second',
          'weight' => '-46',
        ),
        'library_content-blogrss' => array(
          'module' => 'library_content',
          'delta' => 'blogrss',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('blogs');
  $export['blogs'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'front_page';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'library_content-todays_hours' => array(
          'module' => 'library_content',
          'delta' => 'todays_hours',
          'region' => 'branding',
          'weight' => '-10',
        ),
        'libsearch-library_search_bar_front' => array(
          'module' => 'libsearch',
          'delta' => 'library_search_bar_front',
          'region' => 'branding',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;
  $export['front_page'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'site_wide';
  $context->description = 'site wide minus the custom parts using path condition';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '*' => '*',
        '~center_for_gaming_research' => '~center_for_gaming_research',
        '~center-for-gaming-research' => '~center-for-gaming-research',
        '~center_for_gaming_research/*' => '~center_for_gaming_research/*',
        '~center-gaming-research/*' => '~center-gaming-research/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'system-main' => array(
          'module' => 'system',
          'delta' => 'main',
          'region' => 'content',
          'weight' => '-10',
        ),
        'library_content-header_unlv_logo' => array(
          'module' => 'library_content',
          'delta' => 'header_unlv_logo',
          'region' => 'user_first',
          'weight' => '-45',
        ),
        'library_content-header_unlv_search' => array(
          'module' => 'library_content',
          'delta' => 'header_unlv_search',
          'region' => 'user_second',
          'weight' => '-10',
        ),
        'library_content-footer_locations' => array(
          'module' => 'library_content',
          'delta' => 'footer_locations',
          'region' => 'footer_first',
          'weight' => '-10',
        ),
        'library_content-footer_contact' => array(
          'module' => 'library_content',
          'delta' => 'footer_contact',
          'region' => 'footer_first_right',
          'weight' => '-10',
        ),
        'library_content-footer_zed' => array(
          'module' => 'library_content',
          'delta' => 'footer_zed',
          'region' => 'footer_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('site wide minus the custom parts using path condition');
  $export['site_wide'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'site_wide_not_front';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '*' => '*',
        '~<front>' => '~<front>',
        '~center_for_gaming_research' => '~center_for_gaming_research',
        '~center-for-gaming-research' => '~center-for-gaming-research',
        '~center_for_gaming_research/*' => '~center_for_gaming_research/*',
        '~center-gaming-research/*' => '~center-gaming-research/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'libsearch-library_search_bar_front' => array(
          'module' => 'libsearch',
          'delta' => 'library_search_bar_front',
          'region' => 'branding',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;
  $export['site_wide_not_front'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'user_login';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'user' => 'user',
      ),
    ),
    'user' => array(
      'values' => array(
        'anonymous user' => 'anonymous user',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'library_content-login_help' => array(
          'module' => 'library_content',
          'delta' => 'login_help',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;
  $export['user_login'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'user_page';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'user_page' => array(
      'values' => array(
        'view' => 'view',
      ),
      'options' => array(
        'mode' => 'current',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'system-user-menu' => array(
          'module' => 'system',
          'delta' => 'user-menu',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'system-navigation' => array(
          'module' => 'system',
          'delta' => 'navigation',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;
  $export['user_page'] = $context;

  return $export;
}
