<?php
/**
 * @file
 * library_content.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function library_content_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Architecture Studies Library',
    'description' => '',
    'format' => 'html_full',
    'weight' => 0,
    'uuid' => '898b2366-376e-45c4-b254-0f650e3fc2d9',
    'vocabulary_machine_name' => 'page_category',
  );
  return $terms;
}
