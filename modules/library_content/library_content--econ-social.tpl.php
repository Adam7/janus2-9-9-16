<?php
/**
 * Created by JetBrains PhpStorm.
 * User: cgrzecka
 * Date: 10/12/12
 * Time: 10:36 AM
 * @esl: array for links to social thingys
 */
   $esl = array(
     'give' => array(
       'vars' => array(
         'path' => drupal_get_path('theme', 'omegalib'). '/images/econ/give.png',
       ),
       'path' => 'https://netcommunity.unlv.edu/SSLPage.aspx?&pid=294',
     ),
//     't' => array(
//       'vars' => array(
//         'path' => drupal_get_path('theme', 'omegalib'). '/images/econ/twit.png',
//       ),
//       'path' => 'http://twitter.com',
//     ),
//     'fb' => array(
//       'vars' => array(
//         'path' => drupal_get_path('theme', 'omegalib'). '/images/econ/fb.png',
//       ),
//       'path' => 'http://facebook.com',
//     ),
   );
?>
<div id="econ-social-links">
  <?php foreach ($esl as $esli) {
    print l(
      theme('image', $esli['vars']),
      $esli['path'],
      array('attributes' => array('class' => 'econ-img-link'), 'html' => TRUE,)
    );
  } ?>
</div>