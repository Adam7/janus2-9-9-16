<?php
/**
 * @file
 * teacher_development_resources.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function teacher_development_resources_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
}

/**
 * Implements hook_views_api().
 */
function teacher_development_resources_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
