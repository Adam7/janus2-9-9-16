<?php
/**
 * @file
 * teacher_development_resources.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function teacher_development_resources_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'tdrl';
  $context->description = '';
  $context->tag = 'Teacher Development Research Lbrary';
  $context->conditions = array(
    'node_taxonomy' => array(
      'values' => array(
        1631 => 1631,
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
    'path' => array(
      'values' => array(
        'tdrl/*' => 'tdrl/*',
      ),
    ),
  );
  $context->reactions = array();
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Teacher Development Research Lbrary');
  $export['tdrl'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'tdrl_front';
  $context->description = '';
  $context->tag = 'Teacher Development Research Lbrary';
  $context->conditions = array(
    'views' => array(
      'values' => array(
        'tdrl:page' => 'tdrl:page',
      ),
    ),
  );
  $context->reactions = array();
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Teacher Development Research Lbrary');
  $export['tdrl_front'] = $context;

  return $export;
}
