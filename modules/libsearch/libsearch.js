(function ($) {
  $(document).ready(function() {
    // show summon search by default
    $('form#summonsearch').show();
    // fallback for older browsers that do not support placeholder
    //$('div.searchLib input[placeholder]').placeHeld();

    // submit event for IE8 that checks if search terms have been entered
    if (navigator.appVersion.indexOf("MSIE 8.0") > -1 || navigator.appVersion.indexOf("MSIE 9.0") > -1) {
      $('div#searchLib form').submit(function() {
        //get value of the text input field for the form used
        var sv = $('#'+$(this).attr('id')+' input:text').val();
        if (sv == "" ||
          sv == "search books, articles, & more" ||
          sv == "search books" ||
          sv == "search articles" ||
          sv == "search catalog by title" ||
          sv == "author search (e.g., Doe, John)" ||
          sv == "search catalog by subject" ||
          sv == "search this site") {
          alert('Please enter a search term.');
          return false;
        }
      });

      // actions to take on change for IE 8 & 9
      $('select#searchDropdownBox').change(function() {
        // create array that holds the default search texts
        var dst = [
          'search books, articles, & more',
          'search books',
          'search articles',
          'search catalog by title',
          'author search (e.g., Doe, John)',
          'search catalog by subject',
          'search this site'
        ];

        // get search value (sv) by looking at each text input value
        var sv = "";
        if ($.inArray($('input#searchbox_s_q').val(), dst) == -1 ) { sv = $('input#searchbox_s_q').val(); }
        else if ($.inArray($('input#searchbox-title').val(), dst) == -1 ) { sv = $('input#searchbox-title').val(); }
        else if ($.inArray($('input#searchbox-author').val(), dst) == -1 ) { sv = $('input#searchbox-author').val(); }
          else if ($.inArray($('input#searchbox-subject').val(), dst) == -1 ) { sv = $('input#searchbox-subject').val(); }
            else if ($.inArray($('input#searchbox-cse').val(), dst) == -1 ) { sv = $('input#searchbox-cse').val(); }
              else sv = "nosearchterm";

        switch ($(this).val()) {
          case 'quick':
            // clear all values
            $('div#searchLib input[text]').val('');
            // populate latest value
            if (sv != "nosearchterm") { $('input#searchbox_s_q').val(sv); }
            if (sv == "nosearchterm") { $('input#searchbox_s_q').val('search books, articles, & more');	}

            $('input#summonAll').attr('checked', 'checked');
            $('div#searchWrapper form').hide();
            $('form#summonsearch').show();
            break;
          case 'books':
            // clear all values
            $('div#searchLib input[text]').val('');
            // populate latest value
            if (sv != "nosearchterm") { $('input#searchbox_s_q').val(sv); }
            if (sv == "nosearchterm") { $('input#searchbox_s_q').val('search books'); }

            $('input#summonBooks').attr('checked', 'checked');
            $('div#searchWrapper form').hide();
            $('form#summonsearch').show();
            break;
          case 'articles':
            // clear all values
            $('div#searchLib input[text]').val('');
            // populate latest value
            if (sv != "nosearchterm") { $('input#searchbox_s_q').val(sv); }
            if (sv == "nosearchterm") { $('input#searchbox_s_q').val('search articles'); }

            $('input#summonArticles').attr('checked', 'checked');
            $('div#searchWrapper form').hide();
            $('form#summonsearch').show();
            break;
          case 'title':
            // clear all values
            $('div#searchLib input[text]').val('');
            // populate latest value
            if (sv != "nosearchterm") { $('input#searchbox-title').val(sv); }
            if (sv == "nosearchterm") { $('input#searchbox-title').val('search catalog by title'); }

            $('div#searchWrapper form').hide();
            $('form#webpac-title').show();
            break;
          case 'author':
            // clear all values
            $('div#searchLib input[text]').val('');
            // populate latest value
            if (sv != "nosearchterm") { $('input#searchbox-author').val(sv); }
            if (sv == "nosearchterm") { $('input#searchbox-author').val('search catalog by author'); }

            $('div#searchWrapper form').hide();
            $('form#webpac-author').show();
            break;
          case 'subject':
            // clear all values
            $('div#searchLib input[text]').val('');
            // populate latest value
            if (sv != "nosearchterm") { $('input#searchbox-subject').val(sv); }
            if (sv == "nosearchterm") { $('input#searchbox-subject').val('search catalog by subject'); }

            $('div#searchWrapper form').hide();
            $('form#webpac-subject').show();
            break;
          case 'site-search':
            // clear all values
            $('div#searchLib input[text]').val('');
            // populate latest value
            if (sv != "nosearchterm") { $('input#searchbox-cse').val(sv); }
            if (sv == "nosearchterm") { $('input#searchbox-cse').val('search this site'); }

            $('div#searchWrapper form').hide();
            $('form#cse-search-box').show();
            break;
        }

        // clear input on focus
        $('input#searchbox_s_q, ' +
          'input#searchbox-title, ' +
          'input#searchbox-author, ' +
          'input#searchbox-subject, ' +
          'input#searchbox-cse').focus(function() {

            // check if the input value is in dst; if so, clear it on focus
            if ($.inArray($(this).val(), dst) >= 0 ) { $(this).val(''); }
          });
      });
    } else {
      // awesome sauce for real browsers
      $('select#searchDropdownBox').change(function() {
        // check if a value has been entered in any text box and store it in a var
        var searchTerm;
        if ($('input#searchbox_s_q').val().length > 0) { searchTerm = $('input#searchbox_s_q').val(); }
        else if ($('input#searchbox-title').val().length > 0) { searchTerm = $('input#searchbox-title').val(); }
        else if ($('input#searchbox-author').val().length > 0) { searchTerm = $('input#searchbox-author').val(); }
        else if ($('input#searchbox-subject').val().length > 0) { searchTerm = $('input#searchbox-subject').val(); }
        else if ($('input#searchbox-cse').val().length > 0) { searchTerm = $('input#searchbox-cse').val(); }

        switch ($(this).val()) {
          case 'quick':
            // clear all values
            $('div#searchLib input[text]').val('');
            // populate latest value
            $('input#searchbox_s_q').val(searchTerm);
            $('input#searchbox_s_q').attr('placeholder', 'search books, articles, & more');
            $('input#summonAll').attr('checked', 'checked');
            $('div#searchWrapper form').hide();
            $('form#summonsearch').show();
            break;
          case 'books':
            // clear all values
            $('div#searchLib input[text]').val('');
            // populate latest value
            $('input#searchbox_s_q').val(searchTerm);
            $('input#searchbox_s_q').attr('placeholder', 'search books');
            $('input#summonBooks').attr('checked', 'checked');
            $('div#searchWrapper form').hide();
            $('form#summonsearch').show();
            break;
          case 'articles':
            // clear all values
            $('div#searchLib input[text]').val('');
            // populate latest value
            $('input#searchbox_s_q').val(searchTerm);
            $('input#searchbox_s_q').attr('placeholder', 'search articles');
            $('input#summonArticles').attr('checked', 'checked');
            $('div#searchWrapper form').hide();
            $('form#summonsearch').show();
            break;
          case 'title':
            // clear all values
            $('div#searchLib input[text]').val('');
            // populate latest value
            $('input#searchbox-title').val(searchTerm);
            $('div#searchWrapper form').hide();
            $('form#webpac-title').show();
            break;
          case 'author':
            // clear all values
            $('div#searchLib input[text]').val('');
            // populate latest value
            $('input#searchbox-author').val(searchTerm);
            $('div#searchWrapper form').hide();
            $('form#webpac-author').show();
            break;
          case 'subject':
            // clear all values
            $('div#searchLib input[text]').val('');
            // populate latest value
            $('input#searchbox-subject').val(searchTerm);
            $('div#searchWrapper form').hide();
            $('form#webpac-subject').show();
            break;
          case 'site-search':
            // clear all values
            $('div#searchLib input[text]').val('');
            // populate latest value
            $('input#searchbox-cse').val(searchTerm);
            $('div#searchWrapper form').hide();
            $('form#cse-search-box').show();
            break;
        }
      });
    }
  });
})(jQuery);
