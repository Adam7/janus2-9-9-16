<div id="searchWrapper">
  <select id="searchDropdownBox" class="searchSelect" title="Search">
    <option value="quick">Quick Search</option>
    <option value="books">Books</option>
    <option value="articles">Articles</option>
    <option value="title">Catalog: Title</option>
    <option value="author">Catalog: Author</option>
    <option value="subject">Catalog: Keyword</option>
    <option value="site-search">Site Search</option>
  </select>
  <div id="searchLib" class="">
    <div id="summon" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
      <form action="https://unlv.summon.serialssolutions.com/search" id="summonsearch" method="get" enctype="application/x-www-form-urlencoded">
          <input autofocus="focus" class="clearMeFocus" id="searchbox_s_q" name="s.q" type="text" placeholder="search books, articles, & more" value="">
          <input class="button-go" type="submit" value="go" >
          <input checked="checked" id="summonAll" name="s.cmd" type="radio" value="">
          <label for="summonAll">books, articles, and more</label>
          <input id="summonBooks" name="s.cmd" type="radio" value="addFacetValueFilters(ContentType,Book / eBook,f)">
<!--
          <input id="summonBooks" name="s.cmd" type="radio" value="addFacetValueFilters(ContentType,Book,eBook,Web Resource)">
-->
          <label for="summonBooks">only books</label>
          <input id="summonArticles" name="s.cmd" type="radio" value="addFacetValueFilters(ContentType,Journal Article,f|ContentType,Newspaper Article,f)">
<!--
          <input id="summonArticles" name="s.cmd" type="radio" value="addFacetValueFilters(ContentType,Journal Article,Newspaper Article)">
-->
          <label for="summonArticles">only articles</label>
      </form>
    </div>
    <form id="webpac-title" action="http://webpac.library.unlv.edu/search/t" method="get">
      <input type="text" class="clearMeFocus" id="searchbox-title" name="search" placeholder="search catalog by title" value="">
        <option value="1" selected>Entire UNLV Collection</option>
      <input class="button-go" type="submit" value="go">
    </form>
    <form id="webpac-author" action="http://webpac.library.unlv.edu/search/a" method="get">
      <input type="text" class="clearMeFocus" id="searchbox-author" name="search" placeholder="search catalog by author" value="">
        <option value="1" selected>Entire UNLV Collection</option>
      <input class="button-go" type="submit" value="go">
    </form>
    <form id="webpac-subject" action="http://webpac.library.unlv.edu/search/X" method="get">
      <input type="text" class="clearMeFocus" id="searchbox-subject" name="search" placeholder="search catalog by keyword" value="">
        <option value="1" selected>Entire UNLV Collection</option>
      <input class="button-go" type="submit" value="go">
    </form>
    <form action="/search/googleresults.html" id="cse-search-box">
      <input type="hidden" name="cx" value="001581898298350756962:-ch-cv_3h1k" />
      <input type="hidden" name="cof" value="FORID:10" />
      <input type="hidden" name="ie" value="UTF-8" />
      <input id="searchbox-cse" type="text" name="q" size="31" placeholder="search this site" value="" />
      <input class="button-go" type="submit" name="sa" value="go" />
    </form>
  </div>
</div>
