<div class="search-tool-wrapper clearfix">
  <p class="more-research">
    <strong>Research Links: </strong><a href="https://www.library.unlv.edu/search/databases/index.html">List of Databases</a>, <a href="http://www.library.unlv.edu/services/reserves">Course Reserves</a>
  </p>
  <form id="ui_element" class="sb_wrapper clearfix" action="https://unlv.summon.serialssolutions.com/search">
    <p>
      <span id="sb_arrow" class="sb_down"></span>
      <input class="sb_input" type="text" name="s.q" autocomplete="off">
      <input class="sb_search" type="submit" value="">
    </p>
    <ul class="sb_dropdown" style="display: none;">
      <li class="quick-search clearfix" data-value="https://unlv.summon.serialssolutions.com/search">
        <span class="search-type">Quick Search:</span>
        <ul>
          <li>
            <a class="active" href="" data-value="summonAll">Keyword</a>
          </li>
          <li>
            <a href="" data-value="summonBooks">Books</a>
          </li>
          <li>
            <a href="" data-value="summonArticles">Articles</a>
          </li>
        </ul>
        <div class="search-specific" hidden="" style="display:none;">
          <input checked="checked" id="summonAll" name="s.cmd" type="radio" value="">
          <input id="summonBooks" name="s.cmd" type="radio" value="addFacetValueFilters(ContentType,Book / eBook,f)">
          <input id="summonArticles" name="s.cmd" type="radio" value="addFacetValueFilters(ContentType,Journal Article,f|ContentType,Newspaper Article,f)">
<!--
          <input id="summonBooks" name="s.cmd" type="radio" value="addFacetValueFilters(ContentType,Book,eBook,Web Resource)">
          <input id="summonArticles" name="s.cmd" type="radio" value="addFacetValueFilters(ContentType,Journal Article,Newspaper Article)">
-->
        </div>
      </li>
      <li class="catalog-search clearfix">
        <span class="search-type">Catalog:</span>
        <ul>
          <li>
            <a href="" data-value="https://webpac.library.unlv.edu/search~S1/X">Keyword</a>
          </li>
          <li>
            <a href="" data-value="https://webpac.library.unlv.edu/search~S1/t">Title</a>
          </li>
          <li>
            <a href="" data-value="https://webpac.library.unlv.edu/search~S1/a">Author</a>
          </li>
        </ul>
      </li>
      <li class="site-search clearfix" data-value="https://www.library.unlv.edu/search/googleresults.html">
        <span class="search-type">Library Information:</span>
        <ul>
          <li>
            <a href="" name="q">Library Information</a>
          </li>
        </ul>
        <div class="search-specific" hidden="" style="display:none;">
          <input name="cx" value="001581898298350756962:-ch-cv_3h1k" disabled="">
          <input name="cof" value="FORID:10" disabled="">
          <input name="ie" value="UTF-8" disabled="">
        </div>
      </li>
    </ul>
  </form>
  <div class="search-help">
    <a href="" onclick="return false"><span class="glyph"></span></a>
    <span id="tooltip">
      <div>
        <p>
          <strong>Quick Search:</strong> Articles, newspapers, books and ebooks, videos and more. Results primarily available online but may also include books available in the library or articles that can be requested for email delivery from ILLiad.
        </p>
        <p>
          <strong>Catalog:</strong> Books (including ebooks) and other items in UNLV Libraries. Does not search journal articles. 
        </p>
        <p>
          <strong>Library Information:</strong> Pages on library web site, for example research guides, library policies and procedures, hours and events.
        </p>
      </div>
    </span>
  </div>
</div>
