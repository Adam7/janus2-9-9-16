<?php
/**
 * @file
 * lib_permissions.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function lib_permissions_user_default_permissions() {
  $permissions = array();

  // Exported permission: access comments.
  $permissions['access comments'] = array(
    'name' => 'access comments',
    'roles' => array(
      0 => 'administrator',
      1 => 'anonymous user',
      2 => 'authenticated user',
      3 => 'blogger',
    ),
    'module' => 'comment',
  );

  // Exported permission: administer CAPTCHA settings.
  $permissions['administer CAPTCHA settings'] = array(
    'name' => 'administer CAPTCHA settings',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'captcha',
  );

  // Exported permission: administer comment notify.
  $permissions['administer comment notify'] = array(
    'name' => 'administer comment notify',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'comment_notify',
  );

  // Exported permission: administer comments.
  $permissions['administer comments'] = array(
    'name' => 'administer comments',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'comment',
  );

  // Exported permission: administer comments on own blog.
  $permissions['administer comments on own blog'] = array(
    'name' => 'administer comments on own blog',
    'roles' => array(
      0 => 'administrator',
      1 => 'blogger',
    ),
    'module' => 'commentaccess',
  );

  // Exported permission: administer comments on own faq.
  $permissions['administer comments on own faq'] = array(
    'name' => 'administer comments on own faq',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'commentaccess',
  );

  // Exported permission: administer comments on own page.
  $permissions['administer comments on own page'] = array(
    'name' => 'administer comments on own page',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'commentaccess',
  );

  // Exported permission: administer comments on own privilege.
  $permissions['administer comments on own privilege'] = array(
    'name' => 'administer comments on own privilege',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'commentaccess',
  );

  // Exported permission: approve comments on own blog.
  $permissions['approve comments on own blog'] = array(
    'name' => 'approve comments on own blog',
    'roles' => array(
      0 => 'administrator',
      1 => 'blogger',
    ),
    'module' => 'commentaccess',
  );

  // Exported permission: approve comments on own faq.
  $permissions['approve comments on own faq'] = array(
    'name' => 'approve comments on own faq',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'commentaccess',
  );

  // Exported permission: approve comments on own page.
  $permissions['approve comments on own page'] = array(
    'name' => 'approve comments on own page',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'commentaccess',
  );

  // Exported permission: approve comments on own privilege.
  $permissions['approve comments on own privilege'] = array(
    'name' => 'approve comments on own privilege',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'commentaccess',
  );

  // Exported permission: delete comments on own blog.
  $permissions['delete comments on own blog'] = array(
    'name' => 'delete comments on own blog',
    'roles' => array(
      0 => 'administrator',
      1 => 'blogger',
    ),
    'module' => 'commentaccess',
  );

  // Exported permission: delete comments on own faq.
  $permissions['delete comments on own faq'] = array(
    'name' => 'delete comments on own faq',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'commentaccess',
  );

  // Exported permission: delete comments on own page.
  $permissions['delete comments on own page'] = array(
    'name' => 'delete comments on own page',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'commentaccess',
  );

  // Exported permission: delete comments on own privilege.
  $permissions['delete comments on own privilege'] = array(
    'name' => 'delete comments on own privilege',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'commentaccess',
  );

  // Exported permission: edit own comments.
  $permissions['edit own comments'] = array(
    'name' => 'edit own comments',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
      2 => 'blogger',
    ),
    'module' => 'comment',
  );

  // Exported permission: post comments.
  $permissions['post comments'] = array(
    'name' => 'post comments',
    'roles' => array(
      0 => 'administrator',
      1 => 'anonymous user',
      2 => 'authenticated user',
      3 => 'blogger',
    ),
    'module' => 'comment',
  );

  // Exported permission: skip CAPTCHA.
  $permissions['skip CAPTCHA'] = array(
    'name' => 'skip CAPTCHA',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'captcha',
  );

  // Exported permission: skip comment approval.
  $permissions['skip comment approval'] = array(
    'name' => 'skip comment approval',
    'roles' => array(
      0 => 'Helper',
      1 => 'admin',
      2 => 'administrator',
      3 => 'anonymous user',
      4 => 'authenticated user',
      5 => 'blogger',
    ),
    'module' => 'comment',
  );

  // Exported permission: subscribe to comments.
  $permissions['subscribe to comments'] = array(
    'name' => 'subscribe to comments',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'comment_notify',
  );

  return $permissions;
}
