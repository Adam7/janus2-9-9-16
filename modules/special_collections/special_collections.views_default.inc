<?php
/**
 * @file
 * special_collections.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function special_collections_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'special_collections_faq';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Special Collections FAQ';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Table of Contents';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['text'] = '<h2 id="node-[nid]">[title]</h2>';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Edit link */
  $handler->display->display_options['fields']['edit_node']['id'] = 'edit_node';
  $handler->display->display_options['fields']['edit_node']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['edit_node']['field'] = 'edit_node';
  $handler->display->display_options['fields']['edit_node']['label'] = '';
  $handler->display->display_options['fields']['edit_node']['element_label_colon'] = FALSE;
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  /* Field: Content: Category */
  $handler->display->display_options['fields']['field_faq_category']['id'] = 'field_faq_category';
  $handler->display->display_options['fields']['field_faq_category']['table'] = 'field_data_field_faq_category';
  $handler->display->display_options['fields']['field_faq_category']['field'] = 'field_faq_category';
  $handler->display->display_options['fields']['field_faq_category']['label'] = '';
  $handler->display->display_options['fields']['field_faq_category']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_faq_category']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_faq_category']['alter']['text'] = '<h4 id="[field_faq_category-tid]" class="group-speccol-faq-cat">[field_faq_category]</h4>';
  $handler->display->display_options['fields']['field_faq_category']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_faq_category']['type'] = 'taxonomy_term_reference_plain';
  $handler->display->display_options['fields']['field_faq_category']['delta_offset'] = '1';
  $handler->display->display_options['fields']['field_faq_category']['separator'] = '; ';
  /* Field: Content: Tags */
  $handler->display->display_options['fields']['field_faq_tags']['id'] = 'field_faq_tags';
  $handler->display->display_options['fields']['field_faq_tags']['table'] = 'field_data_field_faq_tags';
  $handler->display->display_options['fields']['field_faq_tags']['field'] = 'field_faq_tags';
  $handler->display->display_options['fields']['field_faq_tags']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_faq_tags']['separator'] = '; ';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'faq' => 'faq',
  );
  /* Filter criterion: Content: Category (field_faq_category) */
  $handler->display->display_options['filters']['field_faq_category_tid']['id'] = 'field_faq_category_tid';
  $handler->display->display_options['filters']['field_faq_category_tid']['table'] = 'field_data_field_faq_category';
  $handler->display->display_options['filters']['field_faq_category_tid']['field'] = 'field_faq_category_tid';
  $handler->display->display_options['filters']['field_faq_category_tid']['value'] = array(
    1379 => '1379',
    1380 => '1380',
    1382 => '1382',
    1383 => '1383',
    1384 => '1384',
    1385 => '1385',
    1386 => '1386',
    1387 => '1387',
  );
  $handler->display->display_options['filters']['field_faq_category_tid']['reduce_duplicates'] = TRUE;
  $handler->display->display_options['filters']['field_faq_category_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_faq_category_tid']['vocabulary'] = 'faq_categories';
  $handler->display->display_options['filters']['field_faq_category_tid']['hierarchy'] = 1;

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Special Collections FAQ';
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['grouping'] = array(
    0 => array(
      'field' => 'field_faq_category',
      'rendered' => 1,
      'rendered_strip' => 0,
    ),
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['inline'] = array(
    'title' => 'title',
  );
  $handler->display->display_options['row_options']['hide_empty'] = TRUE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['path'] = 'speccol/about/faq';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Special Collections FAQ';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'menu-special-collections';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['tab_options']['type'] = 'normal';
  $handler->display->display_options['tab_options']['title'] = 'FAQ';
  $handler->display->display_options['tab_options']['weight'] = '0';

  /* Display: Attachment */
  $handler = $view->new_display('attachment', 'Attachment', 'attachment_1');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['grouping'] = array(
    0 => array(
      'field' => 'field_faq_category',
      'rendered' => 1,
      'rendered_strip' => 0,
    ),
  );
  $handler->display->display_options['style_options']['row_class'] = 'group-speccol-faq';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['header'] = FALSE;
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['label'] = 'speccol FAQ header';
  $handler->display->display_options['header']['area']['content'] = '<h3>Categories</h3>';
  $handler->display->display_options['header']['area']['format'] = 'html_full';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Category */
  $handler->display->display_options['fields']['field_faq_category']['id'] = 'field_faq_category';
  $handler->display->display_options['fields']['field_faq_category']['table'] = 'field_data_field_faq_category';
  $handler->display->display_options['fields']['field_faq_category']['field'] = 'field_faq_category';
  $handler->display->display_options['fields']['field_faq_category']['label'] = '';
  $handler->display->display_options['fields']['field_faq_category']['alter']['text'] = '<a href="#[field_faq_category-tid]\'>[field_faq_category-tid]</a>';
  $handler->display->display_options['fields']['field_faq_category']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_faq_category']['alter']['path'] = '#[field_faq_category-tid]';
  $handler->display->display_options['fields']['field_faq_category']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_faq_category']['type'] = 'taxonomy_term_reference_plain';
  $handler->display->display_options['fields']['field_faq_category']['delta_offset'] = '1';
  $handler->display->display_options['fields']['field_faq_category']['separator'] = '; ';
  $handler->display->display_options['displays'] = array(
    'page' => 'page',
    'default' => 0,
  );
  $export['special_collections_faq'] = $view;

  $view = new view();
  $view->name = 'special_collections_featured_items';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'eck_lib_featured_item';
  $view->human_name = 'Special Collections Featured Items';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Welcome to Special Collections';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Field: Link */
  $handler->display->display_options['fields']['field_link']['id'] = 'field_link';
  $handler->display->display_options['fields']['field_link']['table'] = 'field_data_field_link';
  $handler->display->display_options['fields']['field_link']['field'] = 'field_link';
  $handler->display->display_options['fields']['field_link']['label'] = '';
  $handler->display->display_options['fields']['field_link']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_link']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_link']['type'] = 'link_url';
  /* Field: Field: Image */
  $handler->display->display_options['fields']['field_image_pub']['id'] = 'field_image_pub';
  $handler->display->display_options['fields']['field_image_pub']['table'] = 'field_data_field_image_pub';
  $handler->display->display_options['fields']['field_image_pub']['field'] = 'field_image_pub';
  $handler->display->display_options['fields']['field_image_pub']['label'] = '';
  $handler->display->display_options['fields']['field_image_pub']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_image_pub']['alter']['path'] = '[field_link]';
  $handler->display->display_options['fields']['field_image_pub']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_image_pub']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_image_pub']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_image_pub']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  $handler->display->display_options['fields']['field_image_pub']['delta_offset'] = '0';
  /* Field: lib_featured_item: Caption */
  $handler->display->display_options['fields']['field_caption_text']['id'] = 'field_caption_text';
  $handler->display->display_options['fields']['field_caption_text']['table'] = 'field_data_field_caption_text';
  $handler->display->display_options['fields']['field_caption_text']['field'] = 'field_caption_text';
  $handler->display->display_options['fields']['field_caption_text']['label'] = '';
  $handler->display->display_options['fields']['field_caption_text']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_caption_text']['element_default_classes'] = FALSE;
  /* Contextual filter: Global: Null */
  $handler->display->display_options['arguments']['null']['id'] = 'null';
  $handler->display->display_options['arguments']['null']['table'] = 'views';
  $handler->display->display_options['arguments']['null']['field'] = 'null';
  $handler->display->display_options['arguments']['null']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['null']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['null']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['null']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['null']['specify_validation'] = TRUE;
  $handler->display->display_options['arguments']['null']['must_not_be'] = TRUE;
  /* Filter criterion: Lib_featured_item: lib_featured_item type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'eck_lib_featured_item';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'lib_featured_speccol' => 'lib_featured_speccol',
  );
  /* Filter criterion: lib_featured_item: Featured Placement (field_featured_placement_speccol) */
  $handler->display->display_options['filters']['field_featured_placement_speccol_value']['id'] = 'field_featured_placement_speccol_value';
  $handler->display->display_options['filters']['field_featured_placement_speccol_value']['table'] = 'field_data_field_featured_placement_speccol';
  $handler->display->display_options['filters']['field_featured_placement_speccol_value']['field'] = 'field_featured_placement_speccol_value';
  $handler->display->display_options['filters']['field_featured_placement_speccol_value']['value'] = array(
    'featured' => 'featured',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['path'] = 'speccol';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Special Collections';
  $handler->display->display_options['menu']['name'] = 'menu-special-collections';

  /* Display: Block - Special Collections footer slide */
  $handler = $view->new_display('block', 'Block - Special Collections footer slide', 'footer_slide');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Field: Link */
  $handler->display->display_options['fields']['field_link']['id'] = 'field_link';
  $handler->display->display_options['fields']['field_link']['table'] = 'field_data_field_link';
  $handler->display->display_options['fields']['field_link']['field'] = 'field_link';
  $handler->display->display_options['fields']['field_link']['label'] = '';
  $handler->display->display_options['fields']['field_link']['element_type'] = '0';
  $handler->display->display_options['fields']['field_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_link']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_link']['click_sort_column'] = 'url';
  /* Field: Field: Image */
  $handler->display->display_options['fields']['field_image_pub']['id'] = 'field_image_pub';
  $handler->display->display_options['fields']['field_image_pub']['table'] = 'field_data_field_image_pub';
  $handler->display->display_options['fields']['field_image_pub']['field'] = 'field_image_pub';
  $handler->display->display_options['fields']['field_image_pub']['label'] = '';
  $handler->display->display_options['fields']['field_image_pub']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_image_pub']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_image_pub']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_image_pub']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_image_pub']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  $handler->display->display_options['fields']['field_image_pub']['delta_offset'] = '0';
  /* Field: lib_featured_item: Caption */
  $handler->display->display_options['fields']['field_caption_text']['id'] = 'field_caption_text';
  $handler->display->display_options['fields']['field_caption_text']['table'] = 'field_data_field_caption_text';
  $handler->display->display_options['fields']['field_caption_text']['field'] = 'field_caption_text';
  $handler->display->display_options['fields']['field_caption_text']['label'] = '';
  $handler->display->display_options['fields']['field_caption_text']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_caption_text']['element_type'] = 'p';
  $handler->display->display_options['fields']['field_caption_text']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_caption_text']['element_default_classes'] = FALSE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Lib_featured_item: lib_featured_item type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'eck_lib_featured_item';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'lib_featured_speccol' => 'lib_featured_speccol',
  );
  /* Filter criterion: lib_featured_item: Featured Placement (field_featured_placement_speccol) */
  $handler->display->display_options['filters']['field_featured_placement_speccol_value']['id'] = 'field_featured_placement_speccol_value';
  $handler->display->display_options['filters']['field_featured_placement_speccol_value']['table'] = 'field_data_field_featured_placement_speccol';
  $handler->display->display_options['filters']['field_featured_placement_speccol_value']['field'] = 'field_featured_placement_speccol_value';
  $handler->display->display_options['filters']['field_featured_placement_speccol_value']['value'] = array(
    'collection_strengths' => 'collection_strengths',
  );
  $export['special_collections_featured_items'] = $view;

  return $export;
}
