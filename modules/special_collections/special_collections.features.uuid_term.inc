<?php
/**
 * @file
 * special_collections.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function special_collections_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Special Collections',
    'description' => '',
    'format' => 'html_full',
    'weight' => '0',
    'uuid' => '49bd9c5d-36c2-4eba-8812-c8f70c014977',
    'vocabulary_machine_name' => 'page_category',
  );
  return $terms;
}
