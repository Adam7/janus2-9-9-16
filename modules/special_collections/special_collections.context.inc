<?php
/**
 * @file
 * special_collections.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function special_collections_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'special_collections';
  $context->description = 'The section of the site that pertains to Special Collections.';
  $context->tag = 'Special Collections';
  $context->conditions = array(
    'node_taxonomy' => array(
      'values' => array(
        35 => 35,
        1267 => 1267,
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
    'path' => array(
      'values' => array(
        'whats_new_in_special_collections' => 'whats_new_in_special_collections',
        'whats_new_in_special_collections/*' => 'whats_new_in_special_collections/*',
      ),
    ),
    'views' => array(
      'values' => array(
        'special_collections_faq:page' => 'special_collections_faq:page',
        'special_collections_featured_items:page' => 'special_collections_featured_items:page',
      ),
    ),
  );
  $context->reactions = array(
    'delta' => array(
      'delta_template' => 'special_collections',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Special Collections');
  t('The section of the site that pertains to Special Collections.');
  $export['special_collections'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'special_collections_front';
  $context->description = '';
  $context->tag = 'Special Collections';
  $context->conditions = array(
    'views' => array(
      'values' => array(
        'special_collections_featured_items:page' => 'special_collections_featured_items:page',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-813e0298bffab3ede4ebf537d11190b5' => array(
          'module' => 'views',
          'delta' => '813e0298bffab3ede4ebf537d11190b5',
          'region' => 'footer_first',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Special Collections');
  $export['special_collections_front'] = $context;

  return $export;
}
