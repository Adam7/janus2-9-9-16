<?php
/**
 * @file
 * unlv_lib_adconnect.ldap_servers.inc
 */

/**
 * Implements hook_default_ldap_servers().
 */
function unlv_lib_adconnect_default_ldap_servers() {
  $export = array();

  $ldap_servers_conf = new stdClass();
  $ldap_servers_conf->disabled = FALSE; /* Edit this to true to make a default ldap_servers_conf disabled initially */
  $ldap_servers_conf->api_version = 1;
  $ldap_servers_conf->sid = 'opal';
  $ldap_servers_conf->name = 'OPAL';
  $ldap_servers_conf->status = TRUE;
  $ldap_servers_conf->ldap_type = 'ad';
  $ldap_servers_conf->address = 'library.unlv.edu';
  $ldap_servers_conf->port = 389;
  $ldap_servers_conf->tls = FALSE;
  $ldap_servers_conf->bind_method = TRUE;
  $ldap_servers_conf->binddn = 'cn=System No Rights User,ou=Users,ou=Support,dc=library,dc=unlv,dc=edu';
  $ldap_servers_conf->bindpw = 'LTD-1243';
  $ldap_servers_conf->basedn = array(
    0 => 'ou=Users,ou=Support,dc=library,dc=unlv,dc=edu',
    1 => 'ou=Staff,dc=library,dc=unlv,dc=edu',
    2 => 'ou=Users,ou=Lied_Library,ou=Staff,dc=library,dc=unlv,dc=edu',
    3 => 'ou=Test,dc=library,dc=unlv,dc=edu',
  );
  $ldap_servers_conf->user_attr = 'sAMAccountName';
  $ldap_servers_conf->mail_attr = '';
  $ldap_servers_conf->mail_template = '[givenname].[sn]@unlv.edu';
  $ldap_servers_conf->allow_conflicting_drupal_accts = FALSE;
  $ldap_servers_conf->unique_persistent_attr = '';
  $ldap_servers_conf->user_dn_expression = 'cn=%username,%basedn';
  $ldap_servers_conf->ldap_to_drupal_user = '';
  $ldap_servers_conf->testing_drupal_username = 'jwang';
  $ldap_servers_conf->group_object_category = '';
  $ldap_servers_conf->search_pagination = FALSE;
  $ldap_servers_conf->search_page_size = 1000;
  $ldap_servers_conf->weight = 0;
  $export['opal'] = $ldap_servers_conf;

  return $export;
}
