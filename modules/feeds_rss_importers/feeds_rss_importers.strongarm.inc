<?php
/**
 * @file
 * feeds_rss_importers.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function feeds_rss_importers_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_feed_pinterest';
  $strongarm->value = '0';
  $export['comment_anonymous_feed_pinterest'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_feed_pinterest';
  $strongarm->value = 1;
  $export['comment_default_mode_feed_pinterest'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_feed_pinterest';
  $strongarm->value = '50';
  $export['comment_default_per_page_feed_pinterest'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_feed_pinterest';
  $strongarm->value = '1';
  $export['comment_feed_pinterest'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_feed_pinterest';
  $strongarm->value = 1;
  $export['comment_form_location_feed_pinterest'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_feed_pinterest';
  $strongarm->value = '1';
  $export['comment_preview_feed_pinterest'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_feed_pinterest';
  $strongarm->value = 1;
  $export['comment_subject_field_feed_pinterest'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_feed_pinterest';
  $strongarm->value = array();
  $export['menu_options_feed_pinterest'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_feed_pinterest';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_feed_pinterest'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_feed_pinterest';
  $strongarm->value = array(
    0 => 'status',
  );
  $export['node_options_feed_pinterest'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_feed_pinterest';
  $strongarm->value = '0';
  $export['node_preview_feed_pinterest'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_feed_pinterest';
  $strongarm->value = 1;
  $export['node_submitted_feed_pinterest'] = $strongarm;

  return $export;
}
