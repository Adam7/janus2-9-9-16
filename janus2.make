api = 2
core = 7.x

; LIBRARIES
; CKEditor
libraries[ckeditor][download][type] = "get"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%203.6.5/ckeditor_3.6.5.zip"
libraries[ckeditor][directory_name] = "ckeditor"
libraries[ckeditor][destination] = "libraries"

; FlexSlider
libraries[flexslider][download][type] = "get"
libraries[flexslider][download][url] = "https://bitbucket.org/unlv_wads/flexslider/get/a9168b897871.zip"
libraries[flexslider][directory_name] = "flexslider"
libraries[flexslider][destination] = "libraries"

; HeadJS
;libraries[headjs][download][type] = "get"
;libraries[headjs][download][url] = "https://raw.github.com/headjs/headjs/v0.96/dist/head.min.js"
;libraries[headjs][directory_name] = "headjs"
;libraries[headjs][destination] = "libraries"

; MODULES
projects[acquia_connector][version] = 2.14
projects[acquia_connector][subdir] = contrib

projects[admin_menu][version] = "3.0-rc3"
projects[admin_menu][subdir] = contrib 

projects[admin_views][version] = 1.2
projects[admin_views][subdir] = contrib 

;projects[ais][version] = "1.4"
;projects[ais][subdir] = contrib

projects[alias_validation][type] = "module"
projects[alias_validation][download][type] = "git"
projects[alias_validation][download][url] = "https://github.com/z3cka/alias_validation.git"
projects[alias_validation][download][branch] = "7.x-1.0-z3cka"
projects[alias_validation][destination] = contrib

projects[apc_clear][type] = module
projects[apc_clear][download][type] = git
projects[apc_clear][download][url] = "https://github.com/z3cka/apc_clear.git"
projects[apc_clear][download][tag] = 7.x-1.x-beta5
projects[apc_clear][destination] = contrib

projects[backup_migrate][version] = "2.4"
projects[backup_migrate][subdir] = contrib

projects[better_formats][version] = "1.0-beta1"
projects[better_formats][subdir] = contrib

projects[captcha][version] = "1.0-beta2"
projects[captcha][subdir] = contrib

projects[ckeditor][version] = "1.12"
projects[ckeditor][subdir] = contrib

projects[calendar][version] = 3.4
projects[calendar][subdir] = contrib

projects[commentaccess][version] = "1.0"
projects[commentaccess][subdir] = contrib

projects[comment_notify][version] = "1.1"
projects[comment_notify][subdir] = contrib

projects[context][version] = "3.1"
projects[context][subdir] = contrib

projects[ctools][version] = "1.3"
projects[ctools][subdir] = contrib

projects[date][version] = 2.7
projects[date][subdir] = contrib

projects[delta][version] = "3.0-beta11"
projects[delta][subdir] = contrib

projects[diff][version] = 3.2
projects[diff][subdir] = contrib

projects[draggableviews][version] = "2.0"
projects[draggableviews][subdir] = contrib

projects[eck][version] = "2.0-rc1"
projects[eck][subdir] = contrib

projects[entity][version] = "1.0-rc3"
projects[entity][subdir] = contrib

projects[features][version] = "2.10"
projects[features][subdir] = contrib

projects[feeds][version] = "2.0-alpha8"
projects[feeds][subdir] = patched
projects[feeds][patch][] = "http://drupal.org/files/feeds_entity_processor-1033202-217.patch"

projects[feeds_xpathparser][version] = "1.0-beta4"
projects[feeds_xpathparser][subdir] = contrib

; Flexslider using forked repo to remove the recursive make that was downloading the wrong virsion of the flexslider lib
projects[flexslider][download][type] = "git"
projects[flexslider][download][url] = "https://github.com/z3cka/flexslider_drupal.git"
projects[flexslider][download][branch] = "7.x-1.0-rc3-z3cka"
projects[flexslider][destination] = contrib

; old source from drupal.org
;projects[flexslider][version] = "1.0-rc3"
;projects[flexslider][subdir] = contrib

projects[fpa][version] = "2.2"
projects[fpa][subdir] = contrib

projects[globalredirect][version] = "1.5"
projects[globalredirect][subdir] = contrib

projects[google_analytics][version] = 1.4
projects[google_analytics][subdir] = contrib

;projects[headjs][version] = "1.4"
;projects[headjs][subdir] = contrib

projects[image_resize_filter][version] = "1.13"
projects[image_resize_filter][subdir] = contrib

;; version of image_field_caption from github 
projects[image_field_caption][download][tag] = 7.x-1.x-v14.03.1
projects[image_field_caption][download][type] = git
projects[image_field_caption][download][url] = git://github.com/z3cka/image_field_caption.git
projects[image_field_caption][subdir] = contrib
projects[image_field_caption][type] = module

projects[insert][version] = "1.3"
projects[insert][subdir] = contrib

projects[job_scheduler][version] = "2.0-alpha3"
projects[job_scheduler][subdir] = contrib

projects[jquery_update][download][tag] = 7.x-2.x-dev-20140207-1621
projects[jquery_update][download][type] = git
projects[jquery_update][download][url] = git://github.com/z3cka/jquery_update.git
projects[jquery_update][subdir] = dev
projects[jquery_update][type] = module

projects[ldap][version] = "1.0-beta10"
projects[ldap][subdir] = contrib

projects[libraries][version] = "2.0"
projects[libraries][subdir] = contrib

projects[link][version] = "1.0"
projects[link][subdir] = contrib

projects[logintoboggan][version] = "1.3"
projects[logintoboggan][subdir] = contrib

projects[memcache][version] = "1.0"
projects[memcache][subdir] = contrib

projects[mollom][version] = "2.7"
projects[mollom][subdir] = contrib

projects[menu_attributes][version] = "1.0-rc2"
projects[menu_attributes][subdir] = contrib

projects[migrate][version] = "2.4"
projects[migrate][subdir] = contrib

projects[node_export][version] = "3.0"
projects[node_export][subdir] = contrib

projects[omega_tools][version] = "3.0-rc4"
projects[omega_tools][subdir] = contrib

projects[override_node_options][version] = "1.12"
projects[override_node_options][subdir] = contrib

projects[panels][version] = "3.3"
projects[panels][subdir] = contrib

projects[path_alias_xt][version] = "1.0"
projects[path_alias_xt][subdir] = contrib

projects[pathauto][version] = "1.2"
projects[pathauto][subdir] = contrib

projects[print][version] = "1.1"
projects[print][subdir] = contrib

;projects[respondjs][version] = "1.1"
;projects[respondjs][subdir] = contrib

projects[selectmenu][version] = "1.8"
projects[selectmenu][subdir] = contrib

projects[tac_lite][version] = "1.0-rc1"
projects[tac_lite][subdir] = contrib

projects[taxonomy_formatter][version] = "1.3"
projects[taxonomy_formatter][subdir] = contrib

projects[token][subdir] = contrib
projects[token][version] = "1.4"

projects[total_control][version] = "2.3-beta1"
projects[total_control][subdir] = contrib

projects[transliteration][version] = "3.1"
projects[transliteration][subdir] = contrib

projects[special_menu_items][version] = 2.0
projects[special_menu_items][subdir] = contrib

projects[strongarm][version] = "2.0"
projects[strongarm][subdir] = contrib

projects[unlv_events][type] = module
projects[unlv_events][download][type] = git
projects[unlv_events][download][url] = "https://github.com/UNLV-Libraries/unlv_events.git"
projects[unlv_events][download][tag] = 7.x-0.1-beta4
projects[unlv_events][destination] = contrib

projects[uuid][version] = "1.0-alpha5"
projects[uuid][subdir] = contrib

projects[uuid_features][version] = "1.0-alpha3"
projects[uuid_features][subdir] = contrib

projects[uuid_features_menu][type] = module
projects[uuid_features_menu][download][type] = git
projects[uuid_features_menu][download][url] = "https://github.com/z3cka/uuid_features_menu.git"
projects[uuid_features_menu][download][tag] = 7.x-1.0-z3cka
projects[uuid_features_menu][destination] = other

projects[view_unpublished][version] = "1.1"
projects[view_unpublished][subdir] = contrib

; projects[views][version] = "3.5"
projects[views][version] = "3.11"
projects[views][subdir] = contrib

projects[views_bulk_operations][version] = 3.2
projects[views_bulk_operations][subdir] = contrib

projects[views_column_class][version] = "1.0-alpha1"
projects[views_column_class][subdir] = contrib

projects[views_slideshow][version] = "3.0"
projects[views_slideshow][subdir] = contrib

projects[webform][version] = "4.0-beta1"
projects[webform][subdir] = contrib

projects[webform_multiple_file][version] = 1.0-beta3
projects[webform_multiple_file][subdir] = contrib

; THEMES
;projects[oe_theme][type] = "theme"
;projects[oe_theme][download][type] = "git"
;projects[oe_theme][download][url] = "git@bitbucket.org:z3cka/oe-drupal-theme.git"
;projects[oe_theme][download][branch] = "master"


;projects[omega][version] = "3.1"
;projects[omega][subdir] = contrib

projects[omega][type] = "theme"
projects[omega][download][type] = "git"
;projects[omega][download][url] = "git@bitbucket.org:unlv_wads/omega_7.x-3.1.git"
projects[omega][download][url] = "https://bitbucket.org/unlv_wads/omega_7.x-3.1/get/ca0121cd7fae.zip"
projects[omega][download][branch] = "master"
projects[omega][subdir] = contrib
